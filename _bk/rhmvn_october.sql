-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 11, 2022 at 11:43 AM
-- Server version: 10.3.37-MariaDB-cll-lve
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rhmvn_october`
--

-- --------------------------------------------------------

--
-- Table structure for table `alipo_about_posts`
--

CREATE TABLE `alipo_about_posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text NOT NULL,
  `slug` text NOT NULL,
  `short_des` text NOT NULL,
  `des` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `alipo_about_posts`
--

INSERT INTO `alipo_about_posts` (`id`, `title`, `slug`, `short_des`, `des`, `created_at`, `updated_at`) VALUES
(1, 'Lịch sử phát triển Khoa', 'lich-su-phat-trien', 'Khoa Răng Hàm Mặt Trường Đại học Trà Vinh tiền thân là Bộ môn Răng Hàm mặt trực thuộc Khoa Y Dược trường Đại học Trà Vinh,  Bộ môn Răng Hàm Mặt được thành lặp năm 2014, tuyển sinh khóa đầu tiên vào năm 2014', '[{\"block_type\":\"2\",\"title\":\"\",\"full_image\":\"\\/Image\\/baiviet\\/cong-khoa-rhm-tvu-768x281.jpg\",\"right_image\":\"\",\"right_image_2\":\"\",\"left_image\":\"\",\"left_image_2\":\"\",\"left_text\":\"\\r\\n\",\"right_text\":\"\\r\\n\",\"full_vimeo_id\":\"\",\"left_vimeo_id\":\"\",\"right_vimeo_id\":\"\",\"full_text\":\"\\r\\n\",\"youtube_id_left\":\"\",\"youtube_id_right\":\"\",\"youtube_id_full\":\"\"},{\"block_type\":\"12\",\"title\":\"\",\"full_image\":\"\",\"right_image\":\"\",\"right_image_2\":\"\",\"left_image\":\"\",\"left_image_2\":\"\",\"left_text\":\"\\r\\n\",\"right_text\":\"\\r\\n\",\"full_vimeo_id\":\"\",\"left_vimeo_id\":\"\",\"right_vimeo_id\":\"\",\"full_text\":\"<h4>Khoa R\\u0103ng H\\u00e0m M\\u1eb7t Tr\\u01b0\\u1eddng \\u0110\\u1ea1i h\\u1ecdc Tr\\u00e0 Vinh ti\\u1ec1n th\\u00e2n l\\u00e0 B\\u1ed9 m\\u00f4n R\\u0103ng H\\u00e0m m\\u1eb7t tr\\u1ef1c thu\\u1ed9c Khoa Y D\\u01b0\\u1ee3c tr\\u01b0\\u1eddng \\u0110\\u1ea1i h\\u1ecdc Tr\\u00e0 Vinh, &nbsp;B\\u1ed9 m\\u00f4n R\\u0103ng H\\u00e0m M\\u1eb7t \\u0111\\u01b0\\u1ee3c th\\u00e0nh l\\u1eb7p n\\u0103m 2014, tuy\\u1ec3n sinh kh\\u00f3a \\u0111\\u1ea7u ti\\u00ean v\\u00e0o n\\u0103m 2014.<\\/h4>\\r\\n\\r\\n<p>Tr\\u01b0\\u1edbc s\\u1ef1 ph\\u00e1t tri\\u1ec3n c\\u1ee7a ng\\u00e0nh R\\u0103ng H\\u00e0m M\\u1eb7t &nbsp;ng\\u00e0y 08\\/08\\/2018 Hi\\u1ec7u tr\\u01b0\\u1edfng Tr\\u01b0\\u1eddng \\u0110\\u1ea1i h\\u1ecdc Tr\\u00e0 Vinh ra quy\\u1ebft \\u0111\\u1ecbnh th\\u00e0nh l\\u1eadp Khoa R\\u0103ng H\\u00e0m M\\u1eb7t tr\\u00ean c\\u01a1 s\\u1edf ph\\u00e1t tri\\u1ec3n t\\u1eeb b\\u1ed9 m\\u00f4n R\\u0103ng H\\u00e0m M\\u1eb7t thu\\u1ed9c khoa Y D\\u01b0\\u1ee3c v\\u00e0 trao quy\\u1ebft \\u0111\\u1ecbnh \\u0111i\\u1ec1u \\u0111\\u1ed9ng Ti\\u1ebfn s\\u0129 \\u2013 B\\u00e1c s\\u0129 D\\u01b0\\u01a1ng Th\\u1ecb Truy\\u1ec1n l\\u00e0m tr\\u01b0\\u1edfng khoa R\\u0103ng H\\u00e0m M\\u1eb7t v\\u1edbi m\\u1ee5c ti\\u00eau \\u201c\\u0110\\u00e0o t\\u1ea1o B\\u00e1c s\\u1ef9 R\\u0103ng H\\u00e0m M\\u1eb7t (BS RHM) c\\u00f3 y \\u0111\\u1ee9c; c\\u00f3 ki\\u1ebfn th\\u1ee9c v\\u00e0 k\\u1ef9 n\\u0103ng ngh\\u1ec1 nghi\\u1ec7p c\\u01a1 b\\u1ea3n v\\u1ec1 y h\\u1ecdc v\\u00e0 nha khoa \\u0111\\u1ec3 x\\u00e1c \\u0111\\u1ecbnh, \\u0111\\u1ec1 xu\\u1ea5t v\\u00e0 tham gia gi\\u1ea3i quy\\u1ebft c\\u00e1c v\\u1ea5n \\u0111\\u1ec1 trong d\\u1ef1 ph\\u00f2ng, ch\\u1ea9n \\u0111o\\u00e1n v\\u00e0 \\u0111i\\u1ec1u tr\\u1ecb c\\u00e1c b\\u1ec7nh R\\u0103ng H\\u00e0m M\\u1eb7t cho c\\u00e1 nh\\u00e2n v\\u00e0 c\\u1ed9ng \\u0111\\u1ed3ng; c\\u00f3 kh\\u1ea3 n\\u0103ng nghi\\u00ean c\\u1ee9u khoa h\\u1ecdc v\\u00e0 t\\u1ef1 h\\u1ecdc n\\u00e2ng cao tr\\u00ecnh \\u0111\\u1ed9, \\u0111\\u00e1p \\u1ee9ng nhu c\\u1ea7u b\\u1ea3o v\\u1ec7, ch\\u0103m s\\u00f3c s\\u1ee9c kho\\u1ebb r\\u0103ng mi\\u1ec7ng cho nh\\u00e2n d\\u00e2n\\u201d. \\u0110\\u00e2y l\\u00e0 \\u0111i\\u1ec1u ki\\u1ec7n \\u0111\\u1ec3 khoa R\\u0103ng H\\u00e0m M\\u1eb7t ti\\u1ebfp t\\u1ee5c ph\\u00e1t huy vai tr\\u00f2, v\\u1ecb th\\u1ec3 \\u0111\\u1ec3 ph\\u00e1t tri\\u1ec3n v\\u00e0 t\\u1eebng b\\u01b0\\u1edbc tr\\u01b0\\u1edfng th\\u00e0nh.<\\/p>\\r\\n\\r\\n<p>Ng\\u00e0y 08\\/01\\/2019 Tr\\u01b0\\u1eddng \\u0110\\u1ea1i h\\u1ecdc Tr\\u00e0 Vinh t\\u1ed5 ch\\u1ee9c l\\u1ec5 C\\u00f4ng b\\u1ed1 th\\u00e0nh l\\u1eadp khoa R\\u0103ng H\\u00e0m M\\u1eb7t,c\\u00f4ng b\\u1ed1 c\\u00e1c quy\\u1ebft \\u0111\\u1ecbnh b\\u1ed5 nhi\\u1ec7m Tr\\u01b0\\u1edfng khoa, c\\u00f4ng b\\u1ed1 c\\u1ed1 v\\u1ea5n khoa, c\\u00e1c ch\\u1ee9c danh ph\\u00f3 khoa, tr\\u01b0\\u1edfng v\\u00e0 ph\\u00f3 c\\u00e1c b\\u1ed9 m\\u00f4n thu\\u1ed9c khoa R\\u0103ng H\\u00e0m M\\u1eb7t.<\\/p>\\r\\n\\r\\n<p>Ch\\u01b0\\u01a1ng tr\\u00ecnh \\u0111\\u00e0o t\\u1ea1o BS RHM g\\u1ed3m 6 n\\u0103m \\u0111\\u01b0\\u1ee3c c\\u1eadp nh\\u1eadt th\\u01b0\\u1eddng xuy\\u00ean \\u0111\\u1ec3 ph\\u00f9 h\\u1ee3p v\\u1edbi ch\\u01b0\\u01a1ng tr\\u00ecnh \\u0111\\u00e0o t\\u1ea1o trong n\\u01b0\\u1edbc, trong khu v\\u1ef1c v\\u00e0 tr\\u00ean th\\u1ebf gi\\u1edbi. &nbsp;M\\u1ee5c ti\\u00eau c\\u1ee7a ch\\u01b0\\u01a1ng tr\\u00ecnh \\u0111\\u00e0o t\\u1ea1o ph\\u00f9 h\\u1ee3p v\\u1edbi m\\u1ee5c ti\\u00eau Gi\\u00e1o d\\u1ee5c \\u0111\\u1ea1i h\\u1ecdc quy \\u0111\\u1ecbnh t\\u1ea1i Lu\\u1eadt gi\\u00e1o d\\u1ee5c \\u0111\\u1ea1i h\\u1ecdc n\\u0103m 2012 v\\u00e0 s\\u1eeda \\u0111\\u1ed5i b\\u1ed5 sung n\\u0103m 2018. \\u0110\\u1ed3ng th\\u1eddi ho\\u00e0n to\\u00e0n ph\\u00f9 h\\u1ee3p v\\u1edbi s\\u1ee9 m\\u1ec7nh v\\u00e0 t\\u1ea7m nh\\u00ecn ph\\u00e1t tri\\u1ec3n c\\u1ee7a Tr\\u01b0\\u1eddng \\u0110\\u1ea1i h\\u1ecdc Tr\\u00e0 Vinh.<\\/p>\\r\\n\\r\\n<p>Khoa R\\u0103ng H\\u00e0m M\\u1eb7t g\\u1ed3m 7 b\\u1ed9 m\\u00f4n: Nha khoa C\\u01a1 s\\u1edf; B\\u1ec7nh h\\u1ecdc mi\\u1ec7ng v\\u00e0 PTHM; Nha khoa Ph\\u1ee5c h\\u00ecnh; Nha chu; Ch\\u1eefa r\\u0103ng-N\\u1ed9i nha; Nha khoa c\\u00f4ng c\\u1ed9ng; &nbsp;R\\u0103ng tr\\u1ebb em-Ch\\u1ec9nh nha.<\\/p>\\r\\n\\r\\n<p>C\\u00f9ng v\\u1edbi \\u0111\\u1ed9i ng\\u0169 gi\\u1ea3ng vi\\u00ean c\\u01a1 h\\u1eefu g\\u1ed3m 2 Ti\\u1ebfn s\\u0129, 4 BSCKII, 1 BSCKI, 6 ThS, 4 ThS-BSNT, 4 BS RHM.<\\/p>\\r\\n\\r\\n<p>B\\u00ean c\\u1ea1nh \\u0111\\u00f3, Khoa R\\u0103ng H\\u00e0m M\\u1eb7t c\\u00f3 \\u0111\\u1ed9i ng\\u0169 chuy\\u00ean gia l\\u00e0 c\\u00e1c gi\\u1ea3ng vi\\u00ean th\\u1ec9nh gi\\u1ea3ng v\\u1ec1 l\\u00fd thuy\\u1ebft v\\u00e0 th\\u1ef1c h\\u00e0nh \\u0111\\u1ebfn t\\u1eeb Khoa RHM \\u0110HYD TPHCM, B\\u1ec7nh vi\\u1ec7n RHM TPHCM, B\\u1ec7nh vi\\u1ec7n RHM Trung \\u01af\\u01a1ng TPHCM, B\\u1ec7nh vi\\u1ec7n \\u0110a khoa Tr\\u00e0 Vinh, B\\u1ec7nh vi\\u1ec7n Tr\\u01b0\\u1eddng \\u0110H Tr\\u00e0 Vinh gi\\u00fap \\u0111\\u00e0o t\\u1ea1o sinh vi\\u00ean c\\u0169ng nh\\u01b0 h\\u01b0\\u1edbng d\\u1eabn \\u0111\\u1ed9i ng\\u0169 gi\\u1ea3ng vi\\u00ean tr\\u1ebb.<\\/p>\\r\\n\",\"youtube_id_left\":\"\",\"youtube_id_right\":\"\",\"youtube_id_full\":\"\"},{\"block_type\":\"2\",\"title\":\"\",\"full_image\":\"\\/Image\\/baiviet\\/cong-bo-lanh-dao-khoa.jpg\",\"right_image\":\"\",\"right_image_2\":\"\",\"left_image\":\"\",\"left_image_2\":\"\",\"left_text\":\"\\r\\n\",\"right_text\":\"\\r\\n\",\"full_vimeo_id\":\"\",\"left_vimeo_id\":\"\",\"right_vimeo_id\":\"\",\"full_text\":\"\\r\\n\",\"youtube_id_left\":\"\",\"youtube_id_right\":\"\",\"youtube_id_full\":\"\"},{\"block_type\":\"12\",\"title\":\"\",\"full_image\":\"\",\"right_image\":\"\",\"right_image_2\":\"\",\"left_image\":\"\",\"left_image_2\":\"\",\"left_text\":\"\\r\\n\",\"right_text\":\"\\r\\n\",\"full_vimeo_id\":\"\",\"left_vimeo_id\":\"\",\"right_vimeo_id\":\"\",\"full_text\":\"<p style=\\\"text-align: center;\\\"><em>H\\u00ecnh: L\\u1ec5 c\\u00f4ng b\\u1ed1 th\\u00e0nh l\\u1eadp Khoa v\\u00e0 trao quy\\u1ebft \\u0111\\u1ecbnh c\\u00f4ng b\\u1ed1 Ban l\\u00e3nh \\u0111\\u1ea1o khoa ng\\u00e0y 08\\/01\\/2019<\\/em> <\\/p>\\r\\n\",\"youtube_id_left\":\"\",\"youtube_id_right\":\"\",\"youtube_id_full\":\"\"},{\"block_type\":\"2\",\"title\":\"\",\"full_image\":\"\\/Image\\/baiviet\\/ban-co-van.jpg\",\"right_image\":\"\",\"right_image_2\":\"\",\"left_image\":\"\",\"left_image_2\":\"\",\"left_text\":\"\\r\\n\",\"right_text\":\"\\r\\n\",\"full_vimeo_id\":\"\",\"left_vimeo_id\":\"\",\"right_vimeo_id\":\"\",\"full_text\":\"\\r\\n\",\"youtube_id_left\":\"\",\"youtube_id_right\":\"\",\"youtube_id_full\":\"\"},{\"block_type\":\"12\",\"title\":\"\",\"full_image\":\"\",\"right_image\":\"\",\"right_image_2\":\"\",\"left_image\":\"\",\"left_image_2\":\"\",\"left_text\":\"\\r\\n\",\"right_text\":\"\\r\\n\",\"full_vimeo_id\":\"\",\"left_vimeo_id\":\"\",\"right_vimeo_id\":\"\",\"full_text\":\"<p style=\\\"text-align: center;\\\"><em>H\\u00ecnh: Trao quy\\u1ebft \\u0111\\u1ecbnh c\\u00f4ng b\\u1ed1 Ban c\\u1ed1 v\\u1ea5n khoa ng\\u00e0y 08\\/01\\/2019<\\/em> <\\/p>\\r\\n\",\"youtube_id_left\":\"\",\"youtube_id_right\":\"\",\"youtube_id_full\":\"\"},{\"block_type\":\"2\",\"title\":\"\",\"full_image\":\"\\/Image\\/baiviet\\/truong-pho-khoa.jpg\",\"right_image\":\"\",\"right_image_2\":\"\",\"left_image\":\"\",\"left_image_2\":\"\",\"left_text\":\"\\r\\n\",\"right_text\":\"\\r\\n\",\"full_vimeo_id\":\"\",\"left_vimeo_id\":\"\",\"right_vimeo_id\":\"\",\"full_text\":\"\\r\\n\",\"youtube_id_left\":\"\",\"youtube_id_right\":\"\",\"youtube_id_full\":\"\"},{\"block_type\":\"12\",\"title\":\"\",\"full_image\":\"\",\"right_image\":\"\",\"right_image_2\":\"\",\"left_image\":\"\",\"left_image_2\":\"\",\"left_text\":\"\\r\\n\",\"right_text\":\"\\r\\n\",\"full_vimeo_id\":\"\",\"left_vimeo_id\":\"\",\"right_vimeo_id\":\"\",\"full_text\":\"<p style=\\\"text-align: center;\\\"><em>H\\u00ecnh: Trao quy\\u1ebft \\u0111\\u1ecbnh c\\u00f4ng b\\u1ed1 c\\u00e1c Tr\\u01b0\\u1edfng, &nbsp;Ph\\u00f3 b\\u1ed9 m\\u00f4n tr\\u1ef1c thu\\u1ed9c khoa 08\\/01\\/2019<\\/em> <\\/p>\\r\\n\",\"youtube_id_left\":\"\",\"youtube_id_right\":\"\",\"youtube_id_full\":\"\"}]', '2022-12-07 08:29:24', '2022-12-08 07:57:58'),
(2, 'Chức năng nhiệm vụ', 'chuc-nang-nhiem-vu', 'Khoa Răng Hàm Mặt quản lý giảng viên, người lao động khác và người học thuộc khoa theo phân cấp của hiệu trưởng;', '[{\"block_type\":\"2\",\"title\":\"\",\"full_image\":\"\\/Image\\/baiviet\\/SV-nam-4-thuc-hanh-tien-lam-sang-tt-768x512.jpg\",\"right_image\":\"\",\"right_image_2\":\"\",\"left_image\":\"\",\"left_image_2\":\"\",\"left_text\":\"\\r\\n\",\"right_text\":\"\\r\\n\",\"full_vimeo_id\":\"\",\"left_vimeo_id\":\"\",\"right_vimeo_id\":\"\",\"full_text\":\"\\r\\n\",\"youtube_id_left\":\"\",\"youtube_id_right\":\"\",\"youtube_id_full\":\"\"},{\"block_type\":\"12\",\"title\":\"\",\"full_image\":\"\",\"right_image\":\"\",\"right_image_2\":\"\",\"left_image\":\"\",\"left_image_2\":\"\",\"left_text\":\"\\r\\n\",\"right_text\":\"\\r\\n\",\"full_vimeo_id\":\"\",\"left_vimeo_id\":\"\",\"right_vimeo_id\":\"\",\"full_text\":\"<p><strong>Ch\\u1ee9c n\\u0103ng, nhi\\u1ec7m v\\u1ee5<\\/strong><\\/p>\\r\\n\\r\\n<p>Khoa R\\u0103ng Ha\\u0300m M\\u0103\\u0323t qua\\u0309n ly\\u0301 gia\\u0309ng vi\\u00ean, ng\\u01b0\\u01a1\\u0300i lao \\u0111\\u00f4\\u0323ng kha\\u0301c va\\u0300 ng\\u01b0\\u1eddi ho\\u0323c thu\\u00f4\\u0323c khoa theo ph\\u00e2n c\\u00e2\\u0301p cu\\u0309a hi\\u00ea\\u0323u tr\\u01b0\\u01a1\\u0309ng;<\\/p>\\r\\n\\r\\n<p>L\\u1eadp k\\u1ebf ho\\u1ea1ch v\\u00e0 t\\u1ed5 ch\\u1ee9c th\\u1ef1c hi\\u1ec7n c\\u00e1c ho\\u1ea1t \\u0111\\u1ed9ng gi\\u00e1o d\\u1ee5c \\u0111\\u00e0o t\\u1ea1o theo k\\u1ebf ho\\u1ea1ch chung c\\u1ee7a tr\\u01b0\\u1eddng, bao g\\u1ed3m: X\\u00e2y d\\u1ef1ng ch\\u01b0\\u01a1ng tr\\u00ecnh \\u0111\\u00e0o t\\u1ea1o c\\u1ee7a ng\\u00e0nh, chuy\\u00ean ng\\u00e0nh \\u0111\\u01b0\\u1ee3c tr\\u01b0\\u1eddng giao nhi\\u1ec7m v\\u1ee5; t\\u00f4\\u0309 ch\\u1ee9c bi\\u00ean so\\u1ea1n \\u0111\\u1ec1 c\\u01b0\\u01a1ng chi ti\\u00ea\\u0301t c\\u00e1c m\\u00f4n h\\u1ecdc li\\u00ean quan b\\u1ea3o \\u0111\\u1ea3m t\\u00ednh th\\u00f4ng nh\\u00e2\\u0301t, tr\\u00e1nh ch\\u00f4\\u0300ng ch\\u00e9o gi\\u01b0\\u0303a c\\u00e1c m\\u00f4n h\\u1ecdc c\\u1ee7a ch\\u01b0\\u01a1ng tr\\u00ecnh \\u0111\\u00e0o t\\u1ea1o ho\\u1eb7c gi\\u1eefa c\\u00e1c tr\\u00ecnh \\u0111\\u1ed9 \\u0111\\u00e0o t\\u1ea1o; t\\u00f4\\u0309 ch\\u1ee9c ph\\u00e1t tri\\u1ec3n ch\\u01b0\\u01a1ng tr\\u00ecnh \\u0111\\u00e0o t\\u1ea1o, x\\u00e2y d\\u1ef1ng c\\u00e1c \\u0111\\u1ec1 \\u00e1n, bi\\u00ean so\\u1ea1n t\\u00e0i li\\u1ec7u, gi\\u00e1o tr\\u00ecnh theo \\u0111\\u1ecbnh h\\u01b0\\u1edbng ph\\u00e1t tri\\u1ec3n c\\u1ee7a nh\\u00e0 tr\\u01b0\\u1eddng; t\\u1ed5 ch\\u1ee9c nghi\\u00ean c\\u1ee9u c\\u1ea3i ti\\u00ea\\u0301n ph\\u01b0\\u01a1ng ph\\u00e1p gi\\u1ea3ng d\\u1ea1y, h\\u1ecdc t\\u1eadp; x\\u00e2y d\\u1ef1ng v\\u00e0 th\\u1ef1c hi\\u1ec7n ph\\u01b0\\u01a1ng ph\\u00e1p ki\\u00ea\\u0309m tra, \\u0111\\u00e1nh gi\\u00e1 k\\u00ea\\u0301t qu\\u1ea3 h\\u1ecdc t\\u1eadp, r\\u00e8n luy\\u1ec7n c\\u1ee7a sinh vi\\u00ean, b\\u1ea3o \\u0111\\u1ea3m chu\\u00e2\\u0309n \\u0111\\u1ea7u ra c\\u1ee7a ng\\u01b0\\u01a1\\u0300i h\\u1ecdc theo cam k\\u1ebft \\u0111\\u00e3 \\u0111\\u01b0\\u1ee3c c\\u00f4ng b\\u1ed1, \\u0111\\u00e1p \\u1ee9ng nhu c\\u1ea7u s\\u1eed d\\u1ee5ng c\\u1ee7a th\\u1ecb tr\\u01b0\\u1eddng lao \\u0111\\u1ed9ng. Vi\\u1ec7c ph\\u00e1t tri\\u00ea\\u0309n ch\\u01b0\\u01a1ng tr\\u00ecnh \\u0111\\u00e0o t\\u1ea1o, gi\\u00e1o tr\\u00ecnh, t\\u00e0i li\\u1ec7u gi\\u1ea3ng d\\u1ea1y ph\\u1ea3i th\\u1ef1c hi\\u1ec7n theo quy \\u0111\\u1ecbnh c\\u1ee7a B\\u1ed9 Gi\\u00e1o d\\u1ee5c v\\u00e0 \\u0110\\u00e0o t\\u1ea1o;<\\/p>\\r\\n\\r\\n<p>L\\u1eadp k\\u1ebf ho\\u1ea1ch v\\u00e0 t\\u1ed5 ch\\u1ee9c th\\u1ef1c hi\\u1ec7n c\\u00e1c ho\\u1ea1t \\u0111\\u1ed9ng khoa h\\u1ecdc v\\u00e0 c\\u00f4ng ngh\\u1ec7, h\\u1ee3p t\\u00e1c qu\\u00f4\\u0301c t\\u00ea\\u0301; h\\u1ee3p t\\u00e1c v\\u1edbi c\\u00e1c t\\u00f4\\u0309 ch\\u1ee9c khoa h\\u1ecdc v\\u00e0 c\\u00f4ng ngh\\u1ec7, c\\u01a1 s\\u01a1\\u0309 s\\u1ea3n xu\\u00e2\\u0301t, kinh doanh li\\u00ean quan \\u0111\\u00ean ng\\u00e0nh ngh\\u1ec1 \\u0111\\u00e0o t\\u1ea1o v\\u00e0 huy \\u0111\\u1ed9ng s\\u1ef1 tham gia c\\u1ee7a doanh nghi\\u1ec7p v\\u00e0o qu\\u00e1 tr\\u00ecnh \\u0111\\u00e0o t\\u1ea1o c\\u1ee7a khoa;<\\/p>\\r\\n\\r\\n<p>X\\u00e2y d\\u1ef1ng k\\u1ebf ho\\u1ea1ch ph\\u00e1t tri\\u1ec3n \\u0111\\u1ed9i ng\\u0169 gi\\u1ea3ng vi\\u00ean, ng\\u00e0nh ngh\\u1ec1 \\u0111\\u00e0o t\\u1ea1o v\\u00e0 c\\u01a1 s\\u01a1\\u0309 v\\u1eadt ch\\u00e2\\u0301t ph\\u1ee5c v\\u1ee5 cho \\u0111\\u00e0o t\\u1ea1o v\\u00e0 nghi\\u00ean c\\u1ee9u khoa h\\u1ecdc, t\\u0103ng c\\u01b0\\u1eddng \\u0111i\\u00ea\\u0300u ki\\u1ec7n b\\u1ea3o \\u0111\\u1ea3m ch\\u1ea5t l\\u01b0\\u1ee3ng \\u0111\\u00e0o t\\u1ea1o;<\\/p>\\r\\n\\r\\n<p>X\\u00e2y d\\u1ef1ng k\\u1ebf ho\\u1ea1ch v\\u00e0 t\\u1ed5 ch\\u1ee9c th\\u1ef1c hi\\u1ec7n c\\u00f4ng t\\u00e1c gi\\u00e1o d\\u1ee5c ch\\u00ednh tr\\u1ecb, t\\u01b0 t\\u01b0\\u1edfng, \\u0111\\u1ea1o \\u0111\\u1ee9c, l\\u1ed1i s\\u1ed1ng cho gi\\u1ea3ng vi\\u00ean, ng\\u01b0\\u1eddi lao \\u0111\\u1ed9ng, ng\\u01b0\\u1eddi h\\u1ecdc; c\\u00f3 k\\u1ebf ho\\u1ea1ch \\u0111\\u00e0o t\\u1ea1o, b\\u1ed3i d\\u01b0\\u1ee1ng n\\u00e2ng cao tr\\u00ecnh \\u0111\\u1ed9 chuy\\u00ean m\\u00f4n, nghi\\u1ec7p v\\u1ee5 cho gi\\u1ea3ng vi\\u00ean v\\u00e0 ng\\u01b0\\u1eddi lao \\u0111\\u1ed9ng kh\\u00e1c thu\\u1ed9c khoa;<\\/p>\\r\\n\\r\\n<p>T\\u1ed5 ch\\u1ee9c \\u0111\\u00e1nh gi\\u00e1 c\\u00e1n b\\u1ed9 qu\\u1ea3n l\\u00fd, gi\\u1ea3ng vi\\u00ean, nghi\\u00ean c\\u1ee9u vi\\u00ean trong khoa v\\u00e0 tham gia \\u0111\\u00e1nh gi\\u00e1 c\\u00e1n b\\u1ed9 qu\\u1ea3n l\\u00fd trong tr\\u01b0\\u1eddng theo quy \\u0111\\u1ecbnh c\\u1ee7a nh\\u00e0 tr\\u01b0\\u1eddng.<\\/p>\\r\\n\\r\\n<p>V\\u1edbi s\\u1ee9 m\\u1ec7nh \\u0111\\u00e0o t\\u1ea1o ngu\\u1ed3n nh\\u00e2n l\\u1ef1c v\\u1ec1 l\\u0129nh v\\u1ef1c khoa h\\u1ecdc s\\u1ee9c kh\\u1ecfe, ng\\u00e0nh RHM \\u0110H Tr\\u00e0 Vinh \\u0111ang t\\u1ea1o d\\u1ef1ng v\\u00e0 kh\\u1eb3ng \\u0111\\u1ecbnh v\\u1ecb tr\\u00ed c\\u1ee7a m\\u00ecnh trong vi\\u1ec7c \\u0111\\u00e0o t\\u1ea1o \\u0111\\u1ed9i ng\\u0169 B\\u00e1c s\\u0129 RHM ph\\u1ee5c v\\u1ee5 ch\\u0103m s\\u00f3c s\\u1ee9c kh\\u1ecfe r\\u0103ng mi\\u1ec7ng cho nh\\u00e2n d\\u00e2n trong khu v\\u1ef1c \\u0110\\u1ed3ng b\\u1eb1ng s\\u00f4ng C\\u1eedu Long n\\u00f3i ri\\u00eang v\\u00e0 c\\u1ea3 n\\u01b0\\u1edbc n\\u00f3i chung.<\\/p>\\r\\n\\r\\n<p>Khoa RHM x\\u00e2y d\\u1ef1ng t\\u1eebng b\\u01b0\\u1edbc, ho\\u00e0n thi\\u1ec7n n\\u1ed9i dung ch\\u01b0\\u01a1ng tr\\u00ecnh \\u0111\\u00e0o t\\u1ea1o, bi\\u00ean so\\u1ea1n gi\\u00e1o tr\\u00ecnh, t\\u00e0i li\\u1ec7u chuy\\u00ean ng\\u00e0nh, t\\u1ed5 ch\\u1ee9c c\\u00e1c ho\\u1ea1t \\u0111\\u1ed9ng t\\u1eadp hu\\u1ea5n, b\\u1ed3i d\\u01b0\\u1ee1ng \\u0111\\u1ed9i ng\\u0169 c\\u00e1n b\\u1ed9 gi\\u1ea3ng d\\u1ea1y n\\u1eb1m n\\u00e2ng cao ch\\u1ea5t l\\u01b0\\u1ee3ng \\u0111\\u00e0o t\\u1ea1o, khai th\\u00e1c hi\\u1ec7u qu\\u1ea3 c\\u01a1 s\\u1edf h\\u1ea1 t\\u1ea7ng, trang thi\\u1ebft b\\u1ecb nha khoa ph\\u1ee5c v\\u1ee5 c\\u00f4ng t\\u00e1c gi\\u1ea3ng d\\u1ea1y c\\u0169ng nh\\u01b0 \\u0111i\\u1ec1u tr\\u1ecb \\u0111\\u1ea1t hi\\u1ec7u qu\\u1ea3 cao nh\\u1ea5t. V\\u1edbi s\\u1ef1 h\\u01b0\\u1edbng d\\u1eabn t\\u1eadn t\\u00ecnh c\\u1ee7a \\u0111\\u1ed9i ng\\u0169 gi\\u1ea3ng vi\\u00ean nh\\u1eb1m gi\\u00fap cho c\\u00e1c b\\u1ea1n sinh vi\\u00ean c\\u00f3 th\\u1ec3 v\\u1eadn d\\u1ee5ng c\\u00e1c ki\\u1ebfn th\\u1ee9c, k\\u1ef9 n\\u0103ng \\u0111\\u00e3 h\\u1ecdc v\\u00e0o th\\u1ef1c t\\u1ebf l\\u00e2m s\\u00e0ng.<\\/p>\\r\\n\\r\\n<p>Ng\\u00e0nh RHM \\u0111ang t\\u1eebng b\\u01b0\\u1edbc ph\\u00e1t tri\\u1ec3n v\\u00e0 tr\\u01b0\\u1edfng th\\u00e0nh, \\u0111\\u1ea1t \\u0111\\u01b0\\u1ee3c nh\\u1eefng k\\u1ebft qu\\u1ea3 nh\\u1ea5t \\u0111\\u1ecbnh. Khu kh\\u00e1m v\\u00e0 \\u0111i\\u1ec1u tr\\u1ecb RHM tr\\u1ef1c thu\\u1ed9c ph\\u00f2ng kh\\u00e1m \\u0110a khoa m\\u1edf c\\u1eeda \\u0111i\\u1ec3u tr\\u1ecb cho nh\\u00e2n d\\u00e2n t\\u1eeb th\\u00e1ng 7\\/2016 cho \\u0111\\u1ebfn nay \\u0111\\u00e3 khu h\\u00fat \\u0111\\u01b0\\u1ee3c l\\u01b0\\u1ee3ng b\\u1ec7nh nh\\u00e2n ng\\u00e0y c\\u00e0ng \\u0111\\u00f4ng. Khoa RHM \\u0111\\u00e3 v\\u00e0 \\u0111ang cung c\\u1ea5p c\\u00e1c d\\u1ecbch v\\u1ee5 ch\\u0103m s\\u00f3c s\\u1ee9c kh\\u1ecfe r\\u0103ng mi\\u1ec7ng, ph\\u00f2ng ng\\u1eeba v\\u00e0 \\u0111i\\u1ec1u tr\\u1ecb c\\u00e1c b\\u1ec7nh l\\u00fd r\\u0103ng mi\\u1ec7ng, n\\u00e2ng cao ch\\u1ea5t l\\u01b0\\u1ee3ng cu\\u1ed9c s\\u1ed1ng cho nh\\u00e2n d\\u00e2n.<\\/p>\\r\\n\",\"youtube_id_left\":\"\",\"youtube_id_right\":\"\",\"youtube_id_full\":\"\"},{\"block_type\":\"2\",\"title\":\"\",\"full_image\":\"\\/Image\\/baiviet\\/sv-thuc-hanh-tien-lam-sang.jpg\",\"right_image\":\"\",\"right_image_2\":\"\",\"left_image\":\"\",\"left_image_2\":\"\",\"left_text\":\"\\r\\n\",\"right_text\":\"\\r\\n\",\"full_vimeo_id\":\"\",\"left_vimeo_id\":\"\",\"right_vimeo_id\":\"\",\"full_text\":\"\\r\\n\",\"youtube_id_left\":\"\",\"youtube_id_right\":\"\",\"youtube_id_full\":\"\"},{\"block_type\":\"12\",\"title\":\"\",\"full_image\":\"\",\"right_image\":\"\",\"right_image_2\":\"\",\"left_image\":\"\",\"left_image_2\":\"\",\"left_text\":\"\\r\\n\",\"right_text\":\"\\r\\n\",\"full_vimeo_id\":\"\",\"left_vimeo_id\":\"\",\"right_vimeo_id\":\"\",\"full_text\":\"<p style=\\\"text-align: center;\\\"><em>H\\u00ecnh: Sinh vi\\u00ean RHM n\\u0103m 4 th\\u1ef1c h\\u00e0nh tr\\u00ean m\\u00f4 h\\u00ecnh Phantom<\\/em> <\\/p>\\r\\n\",\"youtube_id_left\":\"\",\"youtube_id_right\":\"\",\"youtube_id_full\":\"\"}]', '2022-12-07 17:39:49', '2022-12-08 08:27:24'),
(3, 'Cơ cấu tổ chức', 'co-cau-to-chuc', 'Cơ cấu tổ chức Khoa Răng Hàm Mặt – Trường Đại học Trà Vinh từ 2018 đến nay', '[{\"block_type\":\"2\",\"title\":\"\",\"full_image\":\"\\/Image\\/baiviet\\/C\\u01a1 c\\u1ea5u t\\u1ed5 ch\\u1ee9c.jpg\",\"right_image\":\"\",\"right_image_2\":\"\",\"left_image\":\"\",\"left_image_2\":\"\",\"left_text\":\"\\r\\n\",\"right_text\":\"\\r\\n\",\"full_vimeo_id\":\"\",\"left_vimeo_id\":\"\",\"right_vimeo_id\":\"\",\"full_text\":\"<p>hahaha&nbsp;<\\/p>\\r\\n\",\"youtube_id_left\":\"\",\"youtube_id_right\":\"\",\"youtube_id_full\":\"\"}]', '2022-12-07 20:45:17', '2022-12-08 08:29:08'),
(4, 'Tầm Nhìn Sứ Mạng', 'tam-nhin-su-mang', 'SỨ MẠNG - TẦM NHÌN - GIÁ TRỊ CỐT LÕI', '[{\"block_type\":\"12\",\"title\":\"\",\"full_image\":\"\",\"right_image\":\"\",\"right_image_2\":\"\",\"left_image\":\"\",\"left_image_2\":\"\",\"left_text\":\"\\r\\n\",\"right_text\":\"\\r\\n\",\"full_vimeo_id\":\"\",\"left_vimeo_id\":\"\",\"right_vimeo_id\":\"\",\"full_text\":\"<p>tam-nhin-su-mang<\\/p>\\r\\n\",\"youtube_id_left\":\"\",\"youtube_id_right\":\"\",\"youtube_id_full\":\"\"}]', '2022-12-07 20:50:44', '2022-12-07 20:50:44'),
(5, 'Đội ngũ giảng viên', 'doi-ngu-giang-vien', 'Đội ngũ giảng viên tham gia giảng dạy tại Khoa RHM TVU', '[{\"block_type\":\"12\",\"title\":\"\",\"full_image\":\"\",\"right_image\":\"\",\"right_image_2\":\"\",\"left_image\":\"\",\"left_image_2\":\"\",\"left_text\":\"\\r\\n\",\"right_text\":\"\\r\\n\",\"full_vimeo_id\":\"\",\"left_vimeo_id\":\"\",\"right_vimeo_id\":\"\",\"full_text\":\"<p>\\u0110\\u1ed9i ng\\u0169 gi\\u1ea3ng vi\\u00ean<\\/p>\\r\\n\",\"youtube_id_left\":\"\",\"youtube_id_right\":\"\",\"youtube_id_full\":\"\"}]', '2022-12-07 20:53:41', '2022-12-07 20:53:41'),
(6, 'Cơ sở vật chất', 'co-so-vat-chat', 'Cơ sở vật chất của Khoa Răng Hàm Mặt', '[{\"block_type\":\"12\",\"title\":\"\",\"full_image\":\"\",\"right_image\":\"\",\"right_image_2\":\"\",\"left_image\":\"\",\"left_image_2\":\"\",\"left_text\":\"\\r\\n\",\"right_text\":\"\\r\\n\",\"full_vimeo_id\":\"\",\"left_vimeo_id\":\"\",\"right_vimeo_id\":\"\",\"full_text\":\"<p>co-so-vat-chat<\\/p>\\r\\n\",\"youtube_id_left\":\"\",\"youtube_id_right\":\"\",\"youtube_id_full\":\"\"}]', '2022-12-07 23:30:49', '2022-12-07 23:30:49');

-- --------------------------------------------------------

--
-- Table structure for table `alipo_announce_posts`
--

CREATE TABLE `alipo_announce_posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text NOT NULL,
  `slug` text NOT NULL,
  `short_des` text NOT NULL,
  `des` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `alipo_announce_posts`
--

INSERT INTO `alipo_announce_posts` (`id`, `title`, `slug`, `short_des`, `des`, `created_at`, `updated_at`) VALUES
(1, 'Thông báo tạm dừng tổ chức dạy – học trực tuyến và công tác thi kết thúc học phần đối với các lớp thuộc Khoa Y-Dược và Khoa Răng Hàm Mặt để phòng chống dịch bệnh Covid-19', 'thong-bao-tam-dung-chuc-day-hoc-truc-tuyen-va-cong-tac-thi-ket-thuc-hoc-phan-doi-voi-cac-lop-thuoc-khoa-y-duoc-va-khoa-rang-ham-mat-de-phong-chong-dich-benh-covid-19', 'Thông báo tạm dừng tổ chức dạy – học trực tuyến và công tác thi kết thúc học phần đối với các lớp thuộc Khoa Y-Dược và Khoa Răng Hàm Mặt để phòng chống dịch bệnh Covid-19', '<p><img src=\"https://www.tvu.edu.vn/wp-content/uploads/2021/09/TB-3095-tam-dung-to-chuc-day-va-hoc-truc-tuyen-va-cong-tac-thi-ket-thuc-hoc-phan-cho-sinh-vien-khoa-YD-va-khoa-RHM-de-tham-gia-chong-dich-covid-19-scaled.jpg\" class=\"fr-fic fr-dii\"></p>', '2022-12-07 08:50:46', '2022-12-07 08:50:46'),
(2, 'Đào tạo', 'dao-tao', 'Thông báo đào tạo', '<p>Thông báo đào tạo</p>', '2022-12-08 00:33:53', '2022-12-08 00:33:53'),
(3, 'Khảo thí', 'khao-thi', 'Thông báo Khảo thí', '<p>Thông báo khảo thí</p>', '2022-12-08 00:36:11', '2022-12-08 00:36:11');

-- --------------------------------------------------------

--
-- Table structure for table `alipo_blog_categories`
--

CREATE TABLE `alipo_blog_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text NOT NULL,
  `slug` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `alipo_blog_categories`
--

INSERT INTO `alipo_blog_categories` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Tin tức', 'tin-tuc', '2022-12-07 08:43:56', '2022-12-07 08:43:56'),
(2, 'Đại học', 'dai-hoc', '2022-12-07 23:52:38', '2022-12-07 23:52:38');

-- --------------------------------------------------------

--
-- Table structure for table `alipo_blog_category_posts`
--

CREATE TABLE `alipo_blog_category_posts` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `alipo_blog_category_posts`
--

INSERT INTO `alipo_blog_category_posts` (`category_id`, `post_id`, `created_at`, `updated_at`) VALUES
(2, 1, NULL, NULL),
(1, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `alipo_blog_posts`
--

CREATE TABLE `alipo_blog_posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text NOT NULL,
  `slug` text NOT NULL,
  `short_des` text NOT NULL,
  `des` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `alipo_blog_posts`
--

INSERT INTO `alipo_blog_posts` (`id`, `title`, `slug`, `short_des`, `des`, `created_at`, `updated_at`) VALUES
(1, 'Khám và Điều trị răng miệng miễn phí cho 1000 học sinh tiểu học tại TP Trà Vinh', 'kham-va-dieu-tri-rang-mieng-mien-phi-cho-1000-hoc-sinh-tieu-hoc-tai-tp-tra-vinh', 'Với phương châm “Nâng cao sức khoẻ răng miệng cho cộng đồng”, Khoa Răng Hàm Mặt Trường Đại Học Trà Vinh phối hợp cùng The', '<div dir=\"auto\">Với phương châm “Nâng cao sức khoẻ răng miệng cho cộng đồng”, Khoa Răng Hàm Mặt Trường Đại Học Trà Vinh phối hợp cùng The Future Smiles Foundation (tổ chức phi lợi nhuận tại Hoa Kỳ có sứ mệnh giúp đỡ trẻ em có cơ hội chăm sóc răng miệng) thực hiện dự án “Khám và điều trị răng miệng miễn phí” cho các em học sinh tiểu học trên địa bàn Tỉnh Trà Vinh.</div>\r\n<div dir=\"auto\"><img data-fr-image-pasted=\"true\" src=\"https://rhm.tvu.edu.vn/wp-content/uploads/2022/08/ghe-nha-khoa-300x225.jpg\" alt=\"\" width=\"300\" height=\"225\" srcset=\"https://rhm.tvu.edu.vn/wp-content/uploads/2022/08/ghe-nha-khoa-300x225.jpg 300w, https://rhm.tvu.edu.vn/wp-content/uploads/2022/08/ghe-nha-khoa-1024x768.jpg 1024w, https://rhm.tvu.edu.vn/wp-content/uploads/2022/08/ghe-nha-khoa-768x576.jpg 768w, https://rhm.tvu.edu.vn/wp-content/uploads/2022/08/ghe-nha-khoa-1536x1152.jpg 1536w, https://rhm.tvu.edu.vn/wp-content/uploads/2022/08/ghe-nha-khoa.jpg 2048w\" sizes=\"(max-width: 300px) 100vw, 300px\" class=\"fr-fic fr-dii\"></div>\r\n<div dir=\"auto\">Đặc biệt chương trình sẽ có các phần quà hấp dẫn dành tặng cho mỗi em học sinh khi đến khám và điều trị.</div>\r\n<div dir=\"auto\"><img data-fr-image-pasted=\"true\" src=\"https://rhm.tvu.edu.vn/wp-content/uploads/2022/08/z3630370873173_fe2c53be90c74c375be4a15afe055a6f-300x225.jpg\" alt=\"\" width=\"300\" height=\"225\" srcset=\"https://rhm.tvu.edu.vn/wp-content/uploads/2022/08/z3630370873173_fe2c53be90c74c375be4a15afe055a6f-300x225.jpg 300w, https://rhm.tvu.edu.vn/wp-content/uploads/2022/08/z3630370873173_fe2c53be90c74c375be4a15afe055a6f-1024x768.jpg 1024w, https://rhm.tvu.edu.vn/wp-content/uploads/2022/08/z3630370873173_fe2c53be90c74c375be4a15afe055a6f-768x576.jpg 768w, https://rhm.tvu.edu.vn/wp-content/uploads/2022/08/z3630370873173_fe2c53be90c74c375be4a15afe055a6f-1536x1152.jpg 1536w, https://rhm.tvu.edu.vn/wp-content/uploads/2022/08/z3630370873173_fe2c53be90c74c375be4a15afe055a6f.jpg 2048w\" sizes=\"(max-width: 300px) 100vw, 300px\" class=\"fr-fic fr-dii\"></div>\r\n<div dir=\"auto\">Dự án có sự tham gia của toàn thể Bác sĩ và sinh viên Khoa Răng Hàm Mặt. Rất mong nhận được sự tham gia từ phía phụ huynh và học sinh.</div>\r\n<div dir=\"auto\">Số lượng điều trị: 1000 suất</div>\r\n<div dir=\"auto\">Thời gian dự kiến: từ ngày 25/8 – 26/8/2022</div>\r\n<div dir=\"auto\">Sáng: 7 giờ – 11 giờ</div>\r\n<div dir=\"auto\">Chiềuu: 13 giờ – 17 giờ</div>\r\n<div dir=\"auto\">Địa điểm: Khu khám và điều trị Răng Hàm Mặt Trường Đại học Trà Vinh, số 126 Nguyễn Thiện Thành, K4, P5, TP Trà Vinh</div>\r\n<div dir=\"auto\"><img data-fr-image-pasted=\"true\" src=\"https://rhm.tvu.edu.vn/wp-content/uploads/2022/08/thu-ngo-kham-rhm-229x300.jpg\" alt=\"\" width=\"229\" height=\"300\" srcset=\"https://rhm.tvu.edu.vn/wp-content/uploads/2022/08/thu-ngo-kham-rhm-229x300.jpg 229w, https://rhm.tvu.edu.vn/wp-content/uploads/2022/08/thu-ngo-kham-rhm-782x1024.jpg 782w, https://rhm.tvu.edu.vn/wp-content/uploads/2022/08/thu-ngo-kham-rhm-768x1006.jpg 768w, https://rhm.tvu.edu.vn/wp-content/uploads/2022/08/thu-ngo-kham-rhm.jpg 1170w\" sizes=\"(max-width: 229px) 100vw, 229px\" class=\"fr-fic fr-dii\"></div>', '2022-12-07 08:43:33', '2022-12-07 08:43:33'),
(2, '1000 học sinh tiểu học tại TP Trà Vinh 2', 'kham-va-dieu-tri-rang-mieng-mien-phi-cho-1000-hoc-sinh-tieu-hoc-tai-tp-tra-vinh-2', 'Khám và Điều trị răng miệng miễn phí cho 1000 học sinh tiểu học tại TP Trà Vinh', '<div dir=\"auto\">Với phương châm “Nâng cao sức khoẻ răng miệng cho cộng đồng”, Khoa Răng Hàm Mặt Trường Đại Học Trà Vinh phối hợp cùng The Future Smiles Foundation (tổ chức phi lợi nhuận tại Hoa Kỳ có sứ mệnh giúp đỡ trẻ em có cơ hội chăm sóc răng miệng) thực hiện dự án “Khám và điều trị răng miệng miễn phí” cho các em học sinh tiểu học trên địa bàn Tỉnh Trà Vinh.</div>\r\n<div dir=\"auto\"><img src=\"http://localhost:8888/rhm/storage/app/media/uploaded-files/1670427901699.jpeg\" alt=\"\" width=\"300\" height=\"225\" srcset=\"https://rhm.tvu.edu.vn/wp-content/uploads/2022/08/ghe-nha-khoa-300x225.jpg 300w, https://rhm.tvu.edu.vn/wp-content/uploads/2022/08/ghe-nha-khoa-1024x768.jpg 1024w, https://rhm.tvu.edu.vn/wp-content/uploads/2022/08/ghe-nha-khoa-768x576.jpg 768w, https://rhm.tvu.edu.vn/wp-content/uploads/2022/08/ghe-nha-khoa-1536x1152.jpg 1536w, https://rhm.tvu.edu.vn/wp-content/uploads/2022/08/ghe-nha-khoa.jpg 2048w\" sizes=\"(max-width: 300px) 100vw, 300px\" class=\"fr-fic fr-dii\" data-result=\"success\"></div>\r\n<div dir=\"auto\">Đặc biệt chương trình sẽ có các phần quà hấp dẫn dành tặng cho mỗi em học sinh khi đến khám và điều trị.</div>\r\n<div dir=\"auto\"><img src=\"http://localhost:8888/rhm/storage/app/media/uploaded-files/1670427901668.jpeg\" alt=\"\" width=\"300\" height=\"225\" srcset=\"https://rhm.tvu.edu.vn/wp-content/uploads/2022/08/z3630370873173_fe2c53be90c74c375be4a15afe055a6f-300x225.jpg 300w, https://rhm.tvu.edu.vn/wp-content/uploads/2022/08/z3630370873173_fe2c53be90c74c375be4a15afe055a6f-1024x768.jpg 1024w, https://rhm.tvu.edu.vn/wp-content/uploads/2022/08/z3630370873173_fe2c53be90c74c375be4a15afe055a6f-768x576.jpg 768w, https://rhm.tvu.edu.vn/wp-content/uploads/2022/08/z3630370873173_fe2c53be90c74c375be4a15afe055a6f-1536x1152.jpg 1536w, https://rhm.tvu.edu.vn/wp-content/uploads/2022/08/z3630370873173_fe2c53be90c74c375be4a15afe055a6f.jpg 2048w\" sizes=\"(max-width: 300px) 100vw, 300px\" class=\"fr-fic fr-dii\" data-result=\"success\"></div>\r\n<div dir=\"auto\">Dự án có sự tham gia của toàn thể Bác sĩ và sinh viên Khoa Răng Hàm Mặt. Rất mong nhận được sự tham gia từ phía phụ huynh và học sinh.</div>\r\n<div dir=\"auto\">Số lượng điều trị: 1000 suất</div>\r\n<div dir=\"auto\">Thời gian dự kiến: từ ngày 25/8 – 26/8/2022</div>\r\n<div dir=\"auto\">Sáng: 7 giờ – 11 giờ</div>\r\n<div dir=\"auto\">Chiềuu: 13 giờ – 17 giờ</div>\r\n<div dir=\"auto\">Địa điểm: Khu khám và điều trị Răng Hàm Mặt Trường Đại học Trà Vinh, số 126 Nguyễn Thiện Thành, K4, P5, TP Trà Vinh</div>', '2022-12-07 08:45:03', '2022-12-08 00:32:31');

-- --------------------------------------------------------

--
-- Table structure for table `alipo_cms_aboutpages`
--

CREATE TABLE `alipo_cms_aboutpages` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `alipo_cms_contactpages`
--

CREATE TABLE `alipo_cms_contactpages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `alipo_cms_contact_infos`
--

CREATE TABLE `alipo_cms_contact_infos` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `subject` text NOT NULL,
  `phone` text NOT NULL,
  `message` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `alipo_cms_faqpages`
--

CREATE TABLE `alipo_cms_faqpages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text NOT NULL,
  `intro_title` text NOT NULL,
  `instro_des` text NOT NULL,
  `quote` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `alipo_cms_generals`
--

CREATE TABLE `alipo_cms_generals` (
  `id` int(10) UNSIGNED NOT NULL,
  `phone` text NOT NULL,
  `email` text NOT NULL,
  `address` text NOT NULL,
  `sns` text NOT NULL,
  `subscribe_title` text NOT NULL,
  `footer_des` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `alipo_cms_generals`
--

INSERT INTO `alipo_cms_generals` (`id`, `phone`, `email`, `address`, `sns`, `subscribe_title`, `footer_des`, `created_at`, `updated_at`) VALUES
(1, '02943 855 246 - Số nội bộ: 261', 'khoarhm@tvu.edu.vn', '<p>Khu 1, Trường Đại học Trà Vinh</p>', '[{\"icon\":\"\\/Social network\\/ico-youtube.svg\",\"link\":\"#\"},{\"icon\":\"\\/Social network\\/fb.svg\",\"link\":\"#\"}]', 'Liên hệ ngay', '<p>KHOA RĂNG HÀM MẶT – TRƯỜNG ĐẠI HỌC TRÀ VINH</p>', '2022-12-07 08:16:06', '2022-12-07 08:37:10');

-- --------------------------------------------------------

--
-- Table structure for table `alipo_cms_homepages`
--

CREATE TABLE `alipo_cms_homepages` (
  `id` int(10) UNSIGNED NOT NULL,
  `slider` text NOT NULL,
  `contact_title` text NOT NULL,
  `contact_des` text NOT NULL,
  `intro_title` text NOT NULL,
  `intro_des` text NOT NULL,
  `student_info` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `alipo_cms_homepages`
--

INSERT INTO `alipo_cms_homepages` (`id`, `slider`, `contact_title`, `contact_des`, `intro_title`, `intro_des`, `student_info`, `created_at`, `updated_at`) VALUES
(1, '[{\"image\":\"\\/Image\\/Slider 1.jpg\",\"title\":\"Sinh vi\\u00ean th\\u1ef1c h\\u00e0nh ti\\u1ec1n l\\u00e2m s\\u00e0ng tr\\u00ean phantom\",\"des\":\"Trang thi\\u1ebft b\\u1ecb hi\\u1ec7n \\u0111\\u1ea1i, \\u0111\\u1ed9i ng\\u0169 gi\\u1ea3ng vi\\u00ean t\\u1eadn t\\u00e2m\"},{\"image\":\"\\/Image\\/Slider 2.jpg\",\"title\":\"Sinh vi\\u00ean R\\u0103ng H\\u00e0m M\\u1eb7t kh\\u00f3a \\u0111\\u1ea7u ti\\u00ean ra tr\\u01b0\\u1eddng\",\"des\":\"Kh\\u00f3a \\u0111\\u00e0o t\\u1ea1o \\u0111\\u1ea7u ti\\u00ean v\\u1edbi 60 sinh vi\\u00ean, sau khi ra tr\\u01b0\\u1eddng g\\u00f3p ph\\u1ea7n v\\u00e0o c\\u00f4ng t\\u00e1c \\u0111i\\u1ec1u tr\\u1ecb cho c\\u1ed9ng \\u0111\\u1ed3ng\"}]', 'Liên hệ', '', 'Chào mừng bạn đến với Khoa Răng Hàm Mặt - Trường Đại học Trà Vinh', '<p>Ngày 08/08/2018 Hiệu trưởng Trường ĐH Trà Vinh ra quyết định thành lập Khoa Răng Hàm trên cơ sở phát triển từ BM RHM thuộc khoa Y Dược. Mục tiêu của Trường là “Đào tạo Bác sỹ Răng Hàm Mặt (BS RHM) có y đức; có kiến thức và kỹ năng nghề nghiệp cơ bản về y học và nha khoa…</p>', '[{\"image\":\"\\/icon\\/2702134.png\",\"title\":\"S\\u1ed5 tay sinh vi\\u00ean\",\"des\":\"\",\"link\":\"https:\\/\\/tvusv-my.sharepoint.com\\/personal\\/nttoan_my_tvu_edu_vn\\/_layouts\\/15\\/onedrive.aspx?id=%2Fpersonal%2Fnttoan%5Fmy%5Ftvu%5Fedu%5Fvn%2FDocuments%2FWEB%20RHM%2FBAI%20VIET%2FSO%20TAY%20SINH%20VIEN%20KHOA%20RHM%202021%2Epdf&parent=%2Fpersonal%2Fnttoan%5Fmy%\"},{\"image\":\"\\/icon\\/3221515.png\",\"title\":\"E-Learning \",\"des\":\"\",\"link\":\"https:\\/\\/lms.tvu.edu.vn\\/\"},{\"image\":\"\\/icon\\/9051461.png\",\"title\":\"C\\u1ed5ng TTSV\",\"des\":\"\",\"link\":\"http:\\/\\/ttsv.tvu.edu.vn\\/\"},{\"image\":\"\\/icon\\/9040244.png\",\"title\":\"Di\\u1ec5n \\u0110\\u00e0n Sinh Vi\\u00ean\",\"des\":\"\",\"link\":\"https:\\/\\/sv.rhm.vn\"}]', '2022-12-07 08:23:38', '2022-12-08 05:41:10');

-- --------------------------------------------------------

--
-- Table structure for table `alipo_cms_htqts`
--

CREATE TABLE `alipo_cms_htqts` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `alipo_cms_nckhs`
--

CREATE TABLE `alipo_cms_nckhs` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `alipo_cms_newspages`
--

CREATE TABLE `alipo_cms_newspages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `alipo_cms_servicepages`
--

CREATE TABLE `alipo_cms_servicepages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text NOT NULL,
  `intro_des` text NOT NULL,
  `service_title` text NOT NULL,
  `service_des` text NOT NULL,
  `stats` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `alipo_cms_students`
--

CREATE TABLE `alipo_cms_students` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `alipo_cms_teampages`
--

CREATE TABLE `alipo_cms_teampages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text NOT NULL,
  `team_title` text NOT NULL,
  `team_subtitle` text NOT NULL,
  `team_des` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `alipo_subject_posts`
--

CREATE TABLE `alipo_subject_posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text NOT NULL,
  `slug` text NOT NULL,
  `short_des` text NOT NULL,
  `des` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `alipo_subject_posts`
--

INSERT INTO `alipo_subject_posts` (`id`, `title`, `slug`, `short_des`, `des`, `created_at`, `updated_at`) VALUES
(1, 'Nha Khoa Cơ Sở', 'nha-khoa-co-so', 'Bộ môn Nha Khoa Cơ Sở', '[{\"block_type\":\"12\",\"title\":\"\",\"full_image\":\"\",\"right_image\":\"\",\"right_image_2\":\"\",\"left_image\":\"\",\"left_image_2\":\"\",\"left_text\":\"\\r\\n\",\"right_text\":\"\\r\\n\",\"full_vimeo_id\":\"\",\"left_vimeo_id\":\"\",\"right_vimeo_id\":\"\",\"full_text\":\"<p>nha-khoa-co-so<\\/p>\\r\\n\",\"youtube_id_left\":\"\",\"youtube_id_right\":\"\",\"youtube_id_full\":\"\"}]', '2022-12-07 20:56:22', '2022-12-07 20:56:22'),
(2, 'Bệnh học miệng và phẫu thuật hàm mặt', 'benh-hoc-mieng-va-phau-thuat-ham-mat', 'Bộ môn Bệnh học miệng và phẫu thuật hàm mặt', '[{\"block_type\":\"12\",\"title\":\"\",\"full_image\":\"\",\"right_image\":\"\",\"right_image_2\":\"\",\"left_image\":\"\",\"left_image_2\":\"\",\"left_text\":\"\\r\\n\",\"right_text\":\"\\r\\n\",\"full_vimeo_id\":\"\",\"left_vimeo_id\":\"\",\"right_vimeo_id\":\"\",\"full_text\":\"<p>benh-hoc-mieng-va-phau-thuat-ham-mat<\\/p>\\r\\n\",\"youtube_id_left\":\"\",\"youtube_id_right\":\"\",\"youtube_id_full\":\"\"}]', '2022-12-07 20:59:22', '2022-12-07 20:59:22'),
(3, 'Nha công cộng', 'nha-cong-cong', 'Bộ môn Nha công cộng', '[{\"block_type\":\"12\",\"title\":\"\",\"full_image\":\"\",\"right_image\":\"\",\"right_image_2\":\"\",\"left_image\":\"\",\"left_image_2\":\"\",\"left_text\":\"\\r\\n\",\"right_text\":\"\\r\\n\",\"full_vimeo_id\":\"\",\"left_vimeo_id\":\"\",\"right_vimeo_id\":\"\",\"full_text\":\"<p>nha-cong-cong<\\/p>\\r\\n\",\"youtube_id_left\":\"\",\"youtube_id_right\":\"\",\"youtube_id_full\":\"\"}]', '2022-12-07 23:33:11', '2022-12-07 23:33:11'),
(4, 'Răng trẻ em - Chỉnh nha', 'rang-tre-em-chinh-nha', 'Bộ môn Răng Trẻ em - Chỉnh nha', '[{\"block_type\":\"12\",\"title\":\"\",\"full_image\":\"\",\"right_image\":\"\",\"right_image_2\":\"\",\"left_image\":\"\",\"left_image_2\":\"\",\"left_text\":\"\\r\\n\",\"right_text\":\"\\r\\n\",\"full_vimeo_id\":\"\",\"left_vimeo_id\":\"\",\"right_vimeo_id\":\"\",\"full_text\":\"<p>rang-tre-em-chinh-nha<\\/p>\\r\\n\",\"youtube_id_left\":\"\",\"youtube_id_right\":\"\",\"youtube_id_full\":\"\"}]', '2022-12-07 23:35:06', '2022-12-07 23:35:06'),
(5, 'Chữa răng - Nội nha', 'chua-rang-noi-nha', 'Bộ môn Chữa răng - Nội nha', '[{\"block_type\":\"12\",\"title\":\"\",\"full_image\":\"\",\"right_image\":\"\",\"right_image_2\":\"\",\"left_image\":\"\",\"left_image_2\":\"\",\"left_text\":\"\\r\\n\",\"right_text\":\"\\r\\n\",\"full_vimeo_id\":\"\",\"left_vimeo_id\":\"\",\"right_vimeo_id\":\"\",\"full_text\":\"<p>chua-rang-noi-nha<\\/p>\\r\\n\",\"youtube_id_left\":\"\",\"youtube_id_right\":\"\",\"youtube_id_full\":\"\"}]', '2022-12-07 23:37:04', '2022-12-07 23:37:04'),
(6, 'Nha chu', 'nha-chu', 'Bộ môn Nha chu', '[{\"block_type\":\"12\",\"title\":\"\",\"full_image\":\"\",\"right_image\":\"\",\"right_image_2\":\"\",\"left_image\":\"\",\"left_image_2\":\"\",\"left_text\":\"\\r\\n\",\"right_text\":\"\\r\\n\",\"full_vimeo_id\":\"\",\"left_vimeo_id\":\"\",\"right_vimeo_id\":\"\",\"full_text\":\"<p>nha-chu<\\/p>\\r\\n\",\"youtube_id_left\":\"\",\"youtube_id_right\":\"\",\"youtube_id_full\":\"\"}]', '2022-12-07 23:38:12', '2022-12-07 23:38:12'),
(7, 'Nha khoa phục hình', 'nha-khoa-phuc-hinh', 'Bộ môn Nha khoa phục hình', '[{\"block_type\":\"12\",\"title\":\"\",\"full_image\":\"\",\"right_image\":\"\",\"right_image_2\":\"\",\"left_image\":\"\",\"left_image_2\":\"\",\"left_text\":\"\\r\\n\",\"right_text\":\"\\r\\n\",\"full_vimeo_id\":\"\",\"left_vimeo_id\":\"\",\"right_vimeo_id\":\"\",\"full_text\":\"<p>nha-khoa-phuc-hinh<\\/p>\\r\\n\",\"youtube_id_left\":\"\",\"youtube_id_right\":\"\",\"youtube_id_full\":\"\"}]', '2022-12-07 23:40:47', '2022-12-07 23:40:47');

-- --------------------------------------------------------

--
-- Table structure for table `alipo_training_posts`
--

CREATE TABLE `alipo_training_posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text NOT NULL,
  `slug` text NOT NULL,
  `short_des` text NOT NULL,
  `des` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `alipo_training_posts`
--

INSERT INTO `alipo_training_posts` (`id`, `title`, `slug`, `short_des`, `des`, `created_at`, `updated_at`) VALUES
(1, 'Đại học', 'dai-hoc', 'Đào tạo Đại học', '[{\"block_type\":\"1\",\"title\":\"R\\u0103ng H\\u00e0m M\\u1eb7t\",\"full_image\":\"\",\"right_image\":\"\",\"right_image_2\":\"\",\"left_image\":\"\",\"left_image_2\":\"\",\"left_text\":\"\\r\\n\",\"right_text\":\"\\r\\n\",\"full_vimeo_id\":\"\",\"left_vimeo_id\":\"\",\"right_vimeo_id\":\"\",\"full_text\":\"<p>dai-hoc<\\/p>\\r\\n\",\"youtube_id_left\":\"\",\"youtube_id_right\":\"\",\"youtube_id_full\":\"\"},{\"block_type\":\"12\",\"title\":\"\",\"full_image\":\"\",\"right_image\":\"\",\"right_image_2\":\"\",\"left_image\":\"\",\"left_image_2\":\"\",\"left_text\":\"\\r\\n\",\"right_text\":\"\\r\\n\",\"full_vimeo_id\":\"\",\"left_vimeo_id\":\"\",\"right_vimeo_id\":\"\",\"full_text\":\"<h6><strong>&nbsp;Ch\\u01b0\\u01a1ng Tr\\u00ecnh \\u0110\\u00e0o T\\u1ea1o<\\/strong><\\/h6>\\r\\n\",\"youtube_id_left\":\"\",\"youtube_id_right\":\"\",\"youtube_id_full\":\"\"},{\"block_type\":\"12\",\"title\":\"\",\"full_image\":\"\",\"right_image\":\"\",\"right_image_2\":\"\",\"left_image\":\"\",\"left_image_2\":\"\",\"left_text\":\"\\r\\n\",\"right_text\":\"\\r\\n\",\"full_vimeo_id\":\"\",\"left_vimeo_id\":\"\",\"right_vimeo_id\":\"\",\"full_text\":\"<table style=\\\"width: 67%; margin-right: calc(33%);\\\">\\r\\n\\t<tbody>\\r\\n\\t\\t<tr>\\r\\n\\t\\t\\t<td style=\\\"width: 10.5912%;\\\">\\r\\n\\t\\t\\t\\t<br>\\r\\n\\t\\t\\t<\\/td>\\r\\n\\t\\t\\t<td style=\\\"width: 89.2458%;\\\"><a href=\\\"https:\\/\\/ump.edu.vn\\/uploads\\/ckeditor\\/files\\/BAN%20MO%20TA%20CTDT%20DUA%20TREN%20CNL%202018.pdf\\\">B\\u1ea3n m\\u00f4 t\\u1ea3 ch\\u01b0\\u01a1ng tr\\u00ecnh \\u0111\\u00e0o t\\u1ea1o<\\/a><\\/td>\\r\\n\\t\\t<\\/tr>\\r\\n\\t\\t<tr>\\r\\n\\t\\t\\t<td style=\\\"width: 10.5912%;\\\">\\r\\n\\t\\t\\t\\t<br>\\r\\n\\t\\t\\t<\\/td>\\r\\n\\t\\t\\t<td style=\\\"width: 89.2458%;\\\">\\r\\n\\t\\t\\t\\t<div class=\\\"oc-text-bordered\\\"><a href=\\\"https:\\/\\/ump.edu.vn\\/uploads\\/ckeditor\\/files\\/CHU%CC%9BO%CC%9BNG%20TRINH%20%C4%90A%CC%80O%20TA%CC%A3O%202019.pdf\\\">Ch\\u01b0\\u01a1ng tr\\u00ecnh \\u0111\\u00e0o t\\u1ea1o n\\u0103m 2019<\\/a><a href=\\\"http:\\/\\/admin.ump.edu.vn\\/uploads\\/ckeditor\\/files\\/khung%20chuong%20tr%C3%ACnh\\/n%C4%83m%202016\\/TSDT_DaiHoc_ChuongTrinh_BS_RHM.pdf\\\">&nbsp;<\\/a><\\/div>\\r\\n\\t\\t\\t<\\/td>\\r\\n\\t\\t<\\/tr>\\r\\n\\t\\t<tr>\\r\\n\\t\\t\\t<td style=\\\"width: 10.5912%;\\\">\\r\\n\\t\\t\\t\\t<br>\\r\\n\\t\\t\\t<\\/td>\\r\\n\\t\\t\\t<td style=\\\"width: 89.2458%;\\\">\\r\\n\\t\\t\\t\\t<div class=\\\"oc-text-bordered\\\"><a href=\\\"https:\\/\\/ump.edu.vn\\/uploads\\/ckeditor\\/files\\/CHU%CC%9BO%CC%9BNG%20TRINH%20%C4%90A%CC%80O%20TA%CC%A3O%202019.pdf\\\">Ch\\u01b0\\u01a1ng tr\\u00ecnh \\u0111\\u00e0o t\\u1ea1o n\\u0103m 2020<\\/a><\\/div>\\r\\n\\t\\t\\t<\\/td>\\r\\n\\t\\t<\\/tr>\\r\\n\\t<\\/tbody>\\r\\n<\\/table>\\r\\n\",\"youtube_id_left\":\"\",\"youtube_id_right\":\"\",\"youtube_id_full\":\"\"},{\"block_type\":\"12\",\"title\":\"\",\"full_image\":\"\",\"right_image\":\"\",\"right_image_2\":\"\",\"left_image\":\"\",\"left_image_2\":\"\",\"left_text\":\"\\r\\n\",\"right_text\":\"\\r\\n\",\"full_vimeo_id\":\"\",\"left_vimeo_id\":\"\",\"right_vimeo_id\":\"\",\"full_text\":\"<h6><strong>&nbsp;Chu\\u1ea9n \\u0111\\u1ea7u ra<\\/strong><\\/h6>\\r\\n\",\"youtube_id_left\":\"\",\"youtube_id_right\":\"\",\"youtube_id_full\":\"\"}]', '2022-12-07 23:42:49', '2022-12-08 01:03:21'),
(2, 'Sau đại học', 'sau-dai-hoc', 'Đào tạo Sau đại học', '[{\"block_type\":\"12\",\"title\":\"\",\"full_image\":\"\",\"right_image\":\"\",\"right_image_2\":\"\",\"left_image\":\"\",\"left_image_2\":\"\",\"left_text\":\"\\r\\n\",\"right_text\":\"\\r\\n\",\"full_vimeo_id\":\"\",\"left_vimeo_id\":\"\",\"right_vimeo_id\":\"\",\"full_text\":\"<p>sau-dai-hoc<\\/p>\\r\\n\",\"youtube_id_left\":\"\",\"youtube_id_right\":\"\",\"youtube_id_full\":\"\"}]', '2022-12-07 23:43:40', '2022-12-07 23:43:40'),
(3, 'Đào tạo liên tục', 'dao-tao-lien-tuc', 'Đào tạo liên tục - các khóa ngắn hạn', '[{\"block_type\":\"12\",\"title\":\"\",\"full_image\":\"\",\"right_image\":\"\",\"right_image_2\":\"\",\"left_image\":\"\",\"left_image_2\":\"\",\"left_text\":\"\\r\\n\",\"right_text\":\"\\r\\n\",\"full_vimeo_id\":\"\",\"left_vimeo_id\":\"\",\"right_vimeo_id\":\"\",\"full_text\":\"<p>dao-tao-lien-tuc<\\/p>\\r\\n\",\"youtube_id_left\":\"\",\"youtube_id_right\":\"\",\"youtube_id_full\":\"\"}]', '2022-12-07 23:44:36', '2022-12-07 23:44:36');

-- --------------------------------------------------------

--
-- Table structure for table `backend_access_log`
--

CREATE TABLE `backend_access_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_access_log`
--

INSERT INTO `backend_access_log` (`id`, `user_id`, `ip_address`, `created_at`, `updated_at`) VALUES
(1, 1, '::1', '2022-12-07 06:25:49', '2022-12-07 06:25:49'),
(2, 1, '::1', '2022-12-07 07:01:39', '2022-12-07 07:01:39'),
(3, 1, '27.74.201.136', '2022-12-07 09:02:47', '2022-12-07 09:02:47'),
(4, 1, '123.22.116.150', '2022-12-07 09:05:22', '2022-12-07 09:05:22');

-- --------------------------------------------------------

--
-- Table structure for table `backend_users`
--

CREATE TABLE `backend_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(191) DEFAULT NULL,
  `last_name` varchar(191) DEFAULT NULL,
  `login` varchar(191) NOT NULL,
  `email` varchar(191) NOT NULL,
  `password` varchar(191) NOT NULL,
  `activation_code` varchar(191) DEFAULT NULL,
  `persist_code` varchar(191) DEFAULT NULL,
  `reset_password_code` varchar(191) DEFAULT NULL,
  `permissions` text DEFAULT NULL,
  `is_activated` tinyint(1) NOT NULL DEFAULT 0,
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_users`
--

INSERT INTO `backend_users` (`id`, `first_name`, `last_name`, `login`, `email`, `password`, `activation_code`, `persist_code`, `reset_password_code`, `permissions`, `is_activated`, `role_id`, `activated_at`, `last_login`, `created_at`, `updated_at`, `deleted_at`, `is_superuser`) VALUES
(1, 'Admin', 'Person', 'admin', 'admin@domain.tld', '$2y$10$7xGzvto.68Xr.D1z8tCawOEDI1G4WUMnXsAb3dKNEf.ic8kFnlLuC', NULL, '$2y$10$xmU51/IRBDn488/vy65RY.aFXKvCwa69Rq62gEgJOJ/cTxPbu2CT6', NULL, '', 1, 2, NULL, '2022-12-07 09:05:22', '2022-12-07 06:25:28', '2022-12-07 09:05:22', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `backend_users_groups`
--

CREATE TABLE `backend_users_groups` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_group_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_users_groups`
--

INSERT INTO `backend_users_groups` (`user_id`, `user_group_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `backend_user_groups`
--

CREATE TABLE `backend_user_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `code` varchar(191) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `is_new_user_default` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_user_groups`
--

INSERT INTO `backend_user_groups` (`id`, `name`, `created_at`, `updated_at`, `code`, `description`, `is_new_user_default`) VALUES
(1, 'Owners', '2022-12-07 06:25:28', '2022-12-07 06:25:28', 'owners', 'Default group for website owners.', 0);

-- --------------------------------------------------------

--
-- Table structure for table `backend_user_preferences`
--

CREATE TABLE `backend_user_preferences` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `namespace` varchar(100) NOT NULL,
  `group` varchar(50) NOT NULL,
  `item` varchar(150) NOT NULL,
  `value` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_user_preferences`
--

INSERT INTO `backend_user_preferences` (`id`, `user_id`, `namespace`, `group`, `item`, `value`) VALUES
(1, 1, 'backend', 'reportwidgets', 'dashboard', '{\"welcome\":{\"class\":\"Backend\\\\ReportWidgets\\\\Welcome\",\"sortOrder\":50,\"configuration\":{\"title\":\"Welcome\",\"ocWidgetWidth\":7,\"ocWidgetNewRow\":null}},\"systemStatus\":{\"class\":\"System\\\\ReportWidgets\\\\Status\",\"sortOrder\":60,\"configuration\":{\"ocWidgetWidth\":7}},\"activeTheme\":{\"class\":\"Cms\\\\ReportWidgets\\\\ActiveTheme\",\"sortOrder\":70,\"configuration\":{\"title\":\"Website\",\"ocWidgetWidth\":5,\"ocWidgetNewRow\":null}}}');

-- --------------------------------------------------------

--
-- Table structure for table `backend_user_roles`
--

CREATE TABLE `backend_user_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) NOT NULL,
  `code` varchar(191) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `permissions` text DEFAULT NULL,
  `is_system` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_user_roles`
--

INSERT INTO `backend_user_roles` (`id`, `name`, `code`, `description`, `permissions`, `is_system`, `created_at`, `updated_at`) VALUES
(1, 'Publisher', 'publisher', 'Site editor with access to publishing tools.', '', 1, '2022-12-07 06:25:28', '2022-12-07 06:25:28'),
(2, 'Developer', 'developer', 'Site administrator with access to developer tools.', '', 1, '2022-12-07 06:25:28', '2022-12-07 06:25:28');

-- --------------------------------------------------------

--
-- Table structure for table `backend_user_throttle`
--

CREATE TABLE `backend_user_throttle` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(191) DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT 0,
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `is_suspended` tinyint(1) NOT NULL DEFAULT 0,
  `suspended_at` timestamp NULL DEFAULT NULL,
  `is_banned` tinyint(1) NOT NULL DEFAULT 0,
  `banned_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_user_throttle`
--

INSERT INTO `backend_user_throttle` (`id`, `user_id`, `ip_address`, `attempts`, `last_attempt_at`, `is_suspended`, `suspended_at`, `is_banned`, `banned_at`) VALUES
(1, 1, '::1', 0, NULL, 0, NULL, 0, NULL),
(2, 1, '27.74.201.136', 0, NULL, 0, NULL, 0, NULL),
(3, 1, '123.22.116.150', 0, NULL, 0, NULL, 0, NULL),
(4, 1, '115.79.89.199', 0, NULL, 0, NULL, 0, NULL),
(5, 1, '42.115.95.71', 0, NULL, 0, NULL, 0, NULL),
(6, 1, '115.78.128.5', 0, NULL, 0, NULL, 0, NULL),
(7, 1, '14.226.216.36', 0, NULL, 0, NULL, 0, NULL),
(8, 1, '27.71.99.108', 0, NULL, 0, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `benfreke_menumanager_menus`
--

CREATE TABLE `benfreke_menumanager_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) NOT NULL,
  `description` varchar(191) DEFAULT NULL,
  `url` varchar(191) DEFAULT NULL,
  `nest_left` int(11) DEFAULT NULL,
  `nest_right` int(11) DEFAULT NULL,
  `nest_depth` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_external` tinyint(1) NOT NULL DEFAULT 0,
  `link_target` varchar(191) NOT NULL DEFAULT '_self',
  `enabled` int(11) NOT NULL DEFAULT 1,
  `parameters` varchar(191) DEFAULT NULL,
  `query_string` varchar(191) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `benfreke_menumanager_menus`
--

INSERT INTO `benfreke_menumanager_menus` (`id`, `parent_id`, `title`, `description`, `url`, `nest_left`, `nest_right`, `nest_depth`, `created_at`, `updated_at`, `is_external`, `link_target`, `enabled`, `parameters`, `query_string`) VALUES
(1, NULL, 'main', '', NULL, 1, 52, 0, '2022-12-07 08:16:29', '2022-12-08 09:25:04', 0, '_self', 1, '', ''),
(2, 1, 'Giới thiệu', '', 'gioi-thieu', 2, 15, 1, '2022-12-07 08:17:15', '2022-12-07 23:32:00', 0, '_self', 1, '', ''),
(3, 1, 'Bộ môn', '', 'bo-mon', 16, 31, 1, '2022-12-07 08:17:29', '2022-12-07 23:41:19', 0, '_self', 1, '', ''),
(4, 1, 'Đào tạo', '', 'dao-tao', 32, 39, 1, '2022-12-07 08:17:40', '2022-12-07 23:45:36', 0, '_self', 1, '', ''),
(5, 1, 'Tin tức', '', 'tin-tuc', 42, 43, 1, '2022-12-07 08:17:53', '2022-12-08 09:25:04', 0, '_self', 1, '', ''),
(6, 1, 'Thông báo', '', 'thong-bao', 40, 41, 1, '2022-12-07 08:18:11', '2022-12-08 09:25:04', 0, '_self', 1, '', ''),
(7, 1, 'NCKH', '', 'nckh', 44, 45, 1, '2022-12-07 08:18:24', '2022-12-08 09:25:04', 0, '_self', 1, '', ''),
(8, 1, 'HTQT', '', 'htqt', 46, 47, 1, '2022-12-07 08:18:36', '2022-12-08 09:25:04', 0, '_self', 1, '', ''),
(9, 1, 'Sinh viên – học viên', '', 'sinh-vien-hoc-vien', 48, 49, 1, '2022-12-07 08:18:49', '2022-12-08 09:25:04', 0, '_self', 1, '', ''),
(10, 1, 'Liên hệ', '', 'lien-he', 50, 51, 1, '2022-12-07 08:19:02', '2022-12-08 09:25:04', 0, '_self', 1, '', ''),
(11, 2, 'Lịch sử phát triển', '', 'gioi-thieu-chi-tiet', 3, 4, 2, '2022-12-07 08:30:10', '2022-12-07 08:30:21', 0, '_self', 1, '{\"slug\":\"lich-su-phat-trien\"}', ''),
(12, 2, 'Chức năng nhiệm vụ', '', 'gioi-thieu-chi-tiet', 5, 6, 2, '2022-12-07 20:24:26', '2022-12-07 20:46:36', 0, '_self', 1, '{\"slug\":\"chuc-nang-nhiem-vu\"}', ''),
(13, 2, 'Cơ cấu tổ chức', '', 'gioi-thieu-chi-tiet', 7, 8, 2, '2022-12-07 20:46:19', '2022-12-07 20:49:43', 0, '_self', 1, '{\"slug\":\"co-cau-to-chuc\"}', ''),
(14, 2, 'Tầm nhìn - Sứ mạng', '', 'gioi-thieu-chi-tiet', 9, 10, 2, '2022-12-07 20:51:25', '2022-12-07 20:55:05', 0, '_self', 1, '{\"slug\":\"tam-nhin-su-mang\"}', ''),
(15, 2, 'Đội ngũ giảng viên', '', 'gioi-thieu-chi-tiet', 11, 12, 2, '2022-12-07 20:54:12', '2022-12-07 20:55:18', 0, '_self', 1, '{\"slug\":\"doi-ngu-giang-vien\"}', ''),
(16, 3, 'Nha Khoa Cơ Sở', '', 'bo-mon-chi-tiet', 17, 18, 2, '2022-12-07 20:56:58', '2022-12-07 23:32:00', 0, '_self', 1, '{\"slug\":\"nha-khoa-co-so\"}', ''),
(17, 3, 'Bệnh học miệng và Phẫu thuật hàm mặt', '', 'bo-mon-chi-tiet', 19, 20, 2, '2022-12-07 20:59:59', '2022-12-07 23:32:00', 0, '_self', 1, '{\"slug\":\"benh-hoc-mieng-va-phau-thuat-ham-mat\"}', ''),
(18, 2, 'Cơ sở vật chất', '', 'gioi-thieu-chi-tiet', 13, 14, 2, '2022-12-07 23:31:52', '2022-12-07 23:32:00', 0, '_self', 1, '{\"slug\":\"co-so-vat-chat\"}', ''),
(19, 3, 'Nha công cộng', '', 'bo-mon-chi-tiet', 21, 22, 2, '2022-12-07 23:33:36', '2022-12-07 23:38:28', 0, '_self', 1, '{\"slug\":\"nha-cong-cong\"}', ''),
(20, 3, 'Răng trẻ em - Chỉnh nha', '', 'bo-mon-chi-tiet', 23, 24, 2, '2022-12-07 23:35:51', '2022-12-07 23:35:59', 0, '_self', 1, '{\"slug\":\"rang-tre-em-chinh-nha\"}', ''),
(21, 3, 'Chữa răng - Nội nha', '', 'bo-mon-chi-tiet', 25, 26, 2, '2022-12-07 23:37:37', '2022-12-07 23:37:44', 0, '_self', 1, '{\"slug\":\"chua-rang-noi-nha\"}', ''),
(22, 3, 'Nha chu', '', 'bo-mon-chi-tiet', 27, 28, 2, '2022-12-07 23:39:28', '2022-12-07 23:39:50', 0, '_self', 1, '{\"slug\":\"nha-chu\"}', ''),
(23, 3, 'Nha khoa phục hinh', '', 'bo-mon-chi-tiet', 29, 30, 2, '2022-12-07 23:41:11', '2022-12-07 23:41:19', 0, '_self', 1, '{\"slug\":\"nha-khoa-phuc-hinh\"}', ''),
(24, 4, 'Đại học', '', 'dao-tao-chi-tiet', 33, 34, 2, '2022-12-07 23:43:11', '2022-12-07 23:43:16', 0, '_self', 1, '{\"slug\":\"dai-hoc\"}', ''),
(25, 4, 'Sau đại học', '', 'dao-tao-chi-tiet', 35, 36, 2, '2022-12-07 23:44:03', '2022-12-07 23:44:09', 0, '_self', 1, '{\"slug\":\"sau-dai-hoc\"}', ''),
(26, 4, 'Đào tạo liên tục', '', 'dao-tao-chi-tiet', 37, 38, 2, '2022-12-07 23:45:04', '2022-12-07 23:45:36', 0, '_self', 1, '{\"slug\":\"dao-tao-lien-tuc\"}', '');

-- --------------------------------------------------------

--
-- Table structure for table `cache`
--

CREATE TABLE `cache` (
  `key` varchar(191) NOT NULL,
  `value` longtext NOT NULL,
  `expiration` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_theme_data`
--

CREATE TABLE `cms_theme_data` (
  `id` int(10) UNSIGNED NOT NULL,
  `theme` varchar(191) DEFAULT NULL,
  `data` mediumtext DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_theme_logs`
--

CREATE TABLE `cms_theme_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(20) NOT NULL,
  `theme` varchar(191) DEFAULT NULL,
  `template` varchar(191) DEFAULT NULL,
  `old_template` varchar(191) DEFAULT NULL,
  `content` longtext DEFAULT NULL,
  `old_content` longtext DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_theme_templates`
--

CREATE TABLE `cms_theme_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `source` varchar(191) NOT NULL,
  `path` varchar(191) NOT NULL,
  `content` longtext NOT NULL,
  `file_size` int(10) UNSIGNED NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `deferred_bindings`
--

CREATE TABLE `deferred_bindings` (
  `id` int(10) UNSIGNED NOT NULL,
  `master_type` varchar(191) NOT NULL,
  `master_field` varchar(191) NOT NULL,
  `slave_type` varchar(191) NOT NULL,
  `slave_id` varchar(191) NOT NULL,
  `session_key` varchar(191) NOT NULL,
  `is_bind` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` int(10) UNSIGNED NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` text NOT NULL,
  `exception` longtext DEFAULT NULL,
  `failed_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(191) NOT NULL,
  `payload` text NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2013_10_01_000001_Db_Deferred_Bindings', 1),
(2, '2013_10_01_000002_Db_System_Files', 1),
(3, '2013_10_01_000003_Db_System_Plugin_Versions', 1),
(4, '2013_10_01_000004_Db_System_Plugin_History', 1),
(5, '2013_10_01_000005_Db_System_Settings', 1),
(6, '2013_10_01_000006_Db_System_Parameters', 1),
(7, '2013_10_01_000007_Db_System_Add_Disabled_Flag', 1),
(8, '2013_10_01_000008_Db_System_Mail_Templates', 1),
(9, '2013_10_01_000009_Db_System_Mail_Layouts', 1),
(10, '2014_10_01_000010_Db_Jobs', 1),
(11, '2014_10_01_000011_Db_System_Event_Logs', 1),
(12, '2014_10_01_000012_Db_System_Request_Logs', 1),
(13, '2014_10_01_000013_Db_System_Sessions', 1),
(14, '2015_10_01_000014_Db_System_Mail_Layout_Rename', 1),
(15, '2015_10_01_000015_Db_System_Add_Frozen_Flag', 1),
(16, '2015_10_01_000016_Db_Cache', 1),
(17, '2015_10_01_000017_Db_System_Revisions', 1),
(18, '2015_10_01_000018_Db_FailedJobs', 1),
(19, '2016_10_01_000019_Db_System_Plugin_History_Detail_Text', 1),
(20, '2016_10_01_000020_Db_System_Timestamp_Fix', 1),
(21, '2017_08_04_121309_Db_Deferred_Bindings_Add_Index_Session', 1),
(22, '2017_10_01_000021_Db_System_Sessions_Update', 1),
(23, '2017_10_01_000022_Db_Jobs_FailedJobs_Update', 1),
(24, '2017_10_01_000023_Db_System_Mail_Partials', 1),
(25, '2017_10_23_000024_Db_System_Mail_Layouts_Add_Options_Field', 1),
(26, '2013_10_01_000001_Db_Backend_Users', 2),
(27, '2013_10_01_000002_Db_Backend_User_Groups', 2),
(28, '2013_10_01_000003_Db_Backend_Users_Groups', 2),
(29, '2013_10_01_000004_Db_Backend_User_Throttle', 2),
(30, '2014_01_04_000005_Db_Backend_User_Preferences', 2),
(31, '2014_10_01_000006_Db_Backend_Access_Log', 2),
(32, '2014_10_01_000007_Db_Backend_Add_Description_Field', 2),
(33, '2015_10_01_000008_Db_Backend_Add_Superuser_Flag', 2),
(34, '2016_10_01_000009_Db_Backend_Timestamp_Fix', 2),
(35, '2017_10_01_000010_Db_Backend_User_Roles', 2),
(36, '2018_12_16_000011_Db_Backend_Add_Deleted_At', 2),
(37, '2014_10_01_000001_Db_Cms_Theme_Data', 3),
(38, '2016_10_01_000002_Db_Cms_Timestamp_Fix', 3),
(39, '2017_10_01_000003_Db_Cms_Theme_Logs', 3),
(40, '2018_11_01_000001_Db_Cms_Theme_Templates', 3);

-- --------------------------------------------------------

--
-- Table structure for table `rainlab_translate_attributes`
--

CREATE TABLE `rainlab_translate_attributes` (
  `id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) NOT NULL,
  `model_id` varchar(191) DEFAULT NULL,
  `model_type` varchar(191) DEFAULT NULL,
  `attribute_data` mediumtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rainlab_translate_attributes`
--

INSERT INTO `rainlab_translate_attributes` (`id`, `locale`, `model_id`, `model_type`, `attribute_data`) VALUES
(1, 'en', '1', 'Alipo\\Blog\\Models\\Post', '{\"title\":\"\",\"short_des\":\"\",\"des\":\"\"}'),
(2, 'en', '1', 'Alipo\\Blog\\Models\\Category', '{\"name\":\"\"}'),
(3, 'en', '2', 'Alipo\\Blog\\Models\\Post', '{\"title\":\"\",\"short_des\":\"\",\"des\":\"\"}'),
(4, 'en', '1', 'Alipo\\Cms\\Models\\Homepage', '{\"contact_title\":\"\",\"contact_des\":\"\",\"intro_title\":\"\",\"intro_des\":\"\"}'),
(5, 'en', '1', 'Alipo\\Announce\\Models\\Post', '{\"title\":\"\",\"short_des\":\"\",\"des\":\"\"}'),
(6, 'en', '1', 'Alipo\\About\\Models\\Post', '{\"title\":\"\",\"short_des\":\"\",\"des\":null}'),
(7, 'en', '2', 'Alipo\\About\\Models\\Post', '{\"title\":\"\",\"short_des\":\"\",\"des\":null}'),
(8, 'en', '12', 'BenFreke\\MenuManager\\Models\\Menu', '{\"title\":\"\",\"description\":\"\"}'),
(9, 'en', '3', 'Alipo\\About\\Models\\Post', '{\"title\":\"\",\"short_des\":\"\",\"des\":null}'),
(10, 'en', '13', 'BenFreke\\MenuManager\\Models\\Menu', '{\"title\":\"\",\"description\":\"\"}'),
(11, 'en', '4', 'Alipo\\About\\Models\\Post', '{\"title\":\"\",\"short_des\":\"\",\"des\":null}'),
(12, 'en', '14', 'BenFreke\\MenuManager\\Models\\Menu', '{\"title\":\"\",\"description\":\"\"}'),
(13, 'en', '5', 'Alipo\\About\\Models\\Post', '{\"title\":\"\",\"short_des\":\"\",\"des\":null}'),
(14, 'en', '15', 'BenFreke\\MenuManager\\Models\\Menu', '{\"title\":\"\",\"description\":\"\"}'),
(15, 'en', '1', 'Alipo\\Subject\\Models\\Post', '{\"title\":\"\",\"short_des\":\"\"}'),
(16, 'en', '16', 'BenFreke\\MenuManager\\Models\\Menu', '{\"title\":\"\",\"description\":\"\"}'),
(17, 'en', '2', 'Alipo\\Subject\\Models\\Post', '{\"title\":\"\",\"short_des\":\"\"}'),
(18, 'en', '17', 'BenFreke\\MenuManager\\Models\\Menu', '{\"title\":\"\",\"description\":\"\"}'),
(19, 'en', '6', 'Alipo\\About\\Models\\Post', '{\"title\":\"\",\"short_des\":\"\",\"des\":null}'),
(20, 'en', '18', 'BenFreke\\MenuManager\\Models\\Menu', '{\"title\":\"\",\"description\":\"\"}'),
(21, 'en', '3', 'Alipo\\Subject\\Models\\Post', '{\"title\":\"\",\"short_des\":\"\"}'),
(22, 'en', '19', 'BenFreke\\MenuManager\\Models\\Menu', '{\"title\":\"\",\"description\":\"\"}'),
(23, 'en', '4', 'Alipo\\Subject\\Models\\Post', '{\"title\":\"\",\"short_des\":\"\"}'),
(24, 'en', '20', 'BenFreke\\MenuManager\\Models\\Menu', '{\"title\":\"\",\"description\":\"\"}'),
(25, 'en', '5', 'Alipo\\Subject\\Models\\Post', '{\"title\":\"\",\"short_des\":\"\"}'),
(26, 'en', '21', 'BenFreke\\MenuManager\\Models\\Menu', '{\"title\":\"\",\"description\":\"\"}'),
(27, 'en', '6', 'Alipo\\Subject\\Models\\Post', '{\"title\":\"\",\"short_des\":\"\"}'),
(28, 'en', '22', 'BenFreke\\MenuManager\\Models\\Menu', '{\"title\":\"\",\"description\":\"\"}'),
(29, 'en', '7', 'Alipo\\Subject\\Models\\Post', '{\"title\":\"\",\"short_des\":\"\"}'),
(30, 'en', '23', 'BenFreke\\MenuManager\\Models\\Menu', '{\"title\":\"\",\"description\":\"\"}'),
(31, 'en', '1', 'Alipo\\Training\\Models\\Post', '{\"title\":\"\",\"short_des\":\"\",\"des\":null}'),
(32, 'en', '24', 'BenFreke\\MenuManager\\Models\\Menu', '{\"title\":\"\",\"description\":\"\"}'),
(33, 'en', '2', 'Alipo\\Training\\Models\\Post', '{\"title\":\"\",\"short_des\":\"\",\"des\":null}'),
(34, 'en', '25', 'BenFreke\\MenuManager\\Models\\Menu', '{\"title\":\"\",\"description\":\"\"}'),
(35, 'en', '3', 'Alipo\\Training\\Models\\Post', '{\"title\":\"\",\"short_des\":\"\",\"des\":null}'),
(36, 'en', '26', 'BenFreke\\MenuManager\\Models\\Menu', '{\"title\":\"\",\"description\":\"\"}'),
(37, 'en', '2', 'Alipo\\Blog\\Models\\Category', '{\"name\":\"\"}'),
(38, 'en', '2', 'Alipo\\Announce\\Models\\Post', '{\"title\":\"\",\"short_des\":\"\",\"des\":\"\"}'),
(39, 'en', '27', 'BenFreke\\MenuManager\\Models\\Menu', '{\"title\":\"\",\"description\":\"\"}'),
(40, 'en', '3', 'Alipo\\Announce\\Models\\Post', '{\"title\":\"\",\"short_des\":\"\",\"des\":\"\"}'),
(41, 'en', '28', 'BenFreke\\MenuManager\\Models\\Menu', '{\"title\":\"\",\"description\":\"\"}'),
(42, 'en', '1', 'Utopigs\\Seo\\Models\\Seo', '{\"title\":\"\",\"description\":\"\",\"keywords\":\"\"}');

-- --------------------------------------------------------

--
-- Table structure for table `rainlab_translate_indexes`
--

CREATE TABLE `rainlab_translate_indexes` (
  `id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) NOT NULL,
  `model_id` varchar(191) DEFAULT NULL,
  `model_type` varchar(191) DEFAULT NULL,
  `item` varchar(191) DEFAULT NULL,
  `value` mediumtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rainlab_translate_locales`
--

CREATE TABLE `rainlab_translate_locales` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) NOT NULL,
  `name` varchar(191) DEFAULT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT 0,
  `is_enabled` tinyint(1) NOT NULL DEFAULT 0,
  `sort_order` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rainlab_translate_locales`
--

INSERT INTO `rainlab_translate_locales` (`id`, `code`, `name`, `is_default`, `is_enabled`, `sort_order`) VALUES
(1, 'vi', 'Tiếng Việt', 1, 1, 1),
(2, 'en', 'English', 0, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `rainlab_translate_messages`
--

CREATE TABLE `rainlab_translate_messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) DEFAULT NULL,
  `message_data` mediumtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rainlab_translate_messages`
--

INSERT INTO `rainlab_translate_messages` (`id`, `code`, `message_data`) VALUES
(1, 'họ.tên', '{\"x\":\"H\\u1ecd & T\\u00ean \"}'),
(2, 'email', '{\"x\":\"Email \"}'),
(3, 'tiêu.đề', '{\"x\":\"Ti\\u00eau \\u0111\\u1ec1 \"}'),
(4, 'số.điện.thoại', '{\"x\":\"S\\u1ed1 \\u0111i\\u1ec7n tho\\u1ea1i \"}'),
(5, 'nội.dung', '{\"x\":\"N\\u1ed9i dung\"}'),
(6, 'gửi', '{\"x\":\"G\\u1eedi\"}'),
(7, 'đọc.tiếp', '{\"x\":\"\\u0110\\u1ecdc ti\\u1ebfp\"}'),
(8, 'tin.tức', '{\"x\":\"Tin t\\u1ee9c\"}'),
(9, 'thông.báo', '{\"x\":\"Th\\u00f4ng b\\u00e1o\"}'),
(10, 'gốc.sinh.viên', '{\"x\":\"G\\u1ed1c sinh vi\\u00ean\"}'),
(11, 'en', '{\"x\":\"En\"}'),
(12, 'vi', '{\"x\":\"Vi\"}'),
(13, 'your.email', '{\"x\":\"Your Email \"}'),
(14, 'bộ.môn', '{\"x\":\"B\\u1ed9 m\\u00f4n\"}'),
(15, 'đào.tạo', '{\"x\":\"\\u0110\\u00e0o t\\u1ea1o\"}'),
(16, 'liên.hệ', '{\"x\":\"Li\\u00ean h\\u1ec7\"}'),
(17, 'address', '{\"x\":\"Address\"}'),
(18, 'liên.hệ.ngay', '{\"x\":\"Li\\u00ean h\\u1ec7 ngay\"}'),
(19, 'giới.thiệu', '{\"x\":\"Gi\\u1edbi thi\\u1ec7u\"}'),
(20, 'nghiên.cứu.khoa.học', '{\"x\":\"Nghi\\u00ean c\\u1ee9u khoa h\\u1ecdc\"}'),
(21, 'hợp.tác.quốc.tế', '{\"x\":\"H\\u1ee3p t\\u00e1c qu\\u1ed1c t\\u1ebf\"}'),
(22, 'sinh.viên.học.viên', '{\"x\":\"Sinh vi\\u00ean \\u2013 h\\u1ecdc vi\\u00ean\"}'),
(23, 'related.posts', '{\"x\":\"Related Posts\"}'),
(24, 'tin.liên.quan', '{\"x\":\"Tin li\\u00ean quan\"}'),
(25, 'xem.thêm', '{\"x\":\"Xem th\\u00eam >>\"}');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(191) NOT NULL,
  `payload` text DEFAULT NULL,
  `last_activity` int(11) DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `user_agent` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_event_logs`
--

CREATE TABLE `system_event_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `level` varchar(191) DEFAULT NULL,
  `message` text DEFAULT NULL,
  `details` mediumtext DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_event_logs`
--

INSERT INTO `system_event_logs` (`id`, `level`, `message`, `details`, `created_at`, `updated_at`) VALUES
(1, 'error', 'ErrorException: Trying to get property \'categories\' of non-object in /Applications/MAMP/htdocs/rhm/plugins/alipo/blog/components/Postcpdetail.php:26\nStack trace:\n#0 /Applications/MAMP/htdocs/rhm/plugins/alipo/blog/components/Postcpdetail.php(26): Illuminate\\Foundation\\Bootstrap\\HandleExceptions->handleError(8, \'Trying to get p...\', \'/Applications/M...\', 26, Array)\n#1 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Builder.php(955): Alipo\\Blog\\Components\\Postcpdetail->Alipo\\Blog\\Components\\{closure}(Object(October\\Rain\\Database\\Builder))\n#2 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Concerns/QueriesRelationships.php(47): Illuminate\\Database\\Eloquent\\Builder->callScope(Object(Closure))\n#3 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Concerns/QueriesRelationships.php(131): Illuminate\\Database\\Eloquent\\Builder->has(Object(October\\Rain\\Database\\Relations\\BelongsToMany), \'>=\', 1, \'and\', Object(Closure))\n#4 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Model.php(1477): Illuminate\\Database\\Eloquent\\Builder->whereHas(\'categories\', Object(Closure))\n#5 /Applications/MAMP/htdocs/rhm/vendor/october/rain/src/Extension/ExtendableTrait.php(425): Illuminate\\Database\\Eloquent\\Model->__call(\'whereHas\', Array)\n#6 /Applications/MAMP/htdocs/rhm/vendor/october/rain/src/Database/Model.php(648): October\\Rain\\Database\\Model->extendableCall(\'whereHas\', Array)\n#7 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Model.php(1489): October\\Rain\\Database\\Model->__call(\'whereHas\', Array)\n#8 /Applications/MAMP/htdocs/rhm/plugins/alipo/blog/components/Postcpdetail.php(27): Illuminate\\Database\\Eloquent\\Model::__callStatic(\'whereHas\', Array)\n#9 /Applications/MAMP/htdocs/rhm/modules/cms/Classes/CmsCompoundObject.php(168): Alipo\\Blog\\Components\\Postcpdetail->onRun()\n#10 /Applications/MAMP/htdocs/rhm/modules/cms/Classes/Controller.php(499): Cms\\Classes\\CmsCompoundObject->runComponents()\n#11 /Applications/MAMP/htdocs/rhm/modules/cms/Classes/Controller.php(392): Cms\\Classes\\Controller->execPageCycle()\n#12 /Applications/MAMP/htdocs/rhm/modules/cms/Classes/Controller.php(223): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#13 /Applications/MAMP/htdocs/rhm/modules/cms/Classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'tin-tuc/default\')\n#14 [internal function]: Cms\\Classes\\CmsController->run(\'tin-tuc/default\')\n#15 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#16 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#17 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#18 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#19 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#20 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#21 /Applications/MAMP/htdocs/rhm/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#22 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#23 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#24 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#25 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#26 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#28 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#29 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#31 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#32 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#35 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#41 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#42 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#43 /Applications/MAMP/htdocs/rhm/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#44 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#45 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#47 /Applications/MAMP/htdocs/rhm/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(24): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#48 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#49 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#50 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#51 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#52 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#53 /Applications/MAMP/htdocs/rhm/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#54 {main}', NULL, '2022-12-07 06:52:47', '2022-12-07 06:52:47'),
(2, 'error', 'ErrorException: Trying to get property \'categories\' of non-object in /Applications/MAMP/htdocs/rhm/plugins/alipo/blog/components/Postcpdetail.php:26\nStack trace:\n#0 /Applications/MAMP/htdocs/rhm/plugins/alipo/blog/components/Postcpdetail.php(26): Illuminate\\Foundation\\Bootstrap\\HandleExceptions->handleError(8, \'Trying to get p...\', \'/Applications/M...\', 26, Array)\n#1 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Builder.php(955): Alipo\\Blog\\Components\\Postcpdetail->Alipo\\Blog\\Components\\{closure}(Object(October\\Rain\\Database\\Builder))\n#2 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Concerns/QueriesRelationships.php(47): Illuminate\\Database\\Eloquent\\Builder->callScope(Object(Closure))\n#3 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Concerns/QueriesRelationships.php(131): Illuminate\\Database\\Eloquent\\Builder->has(Object(October\\Rain\\Database\\Relations\\BelongsToMany), \'>=\', 1, \'and\', Object(Closure))\n#4 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Model.php(1477): Illuminate\\Database\\Eloquent\\Builder->whereHas(\'categories\', Object(Closure))\n#5 /Applications/MAMP/htdocs/rhm/vendor/october/rain/src/Extension/ExtendableTrait.php(425): Illuminate\\Database\\Eloquent\\Model->__call(\'whereHas\', Array)\n#6 /Applications/MAMP/htdocs/rhm/vendor/october/rain/src/Database/Model.php(648): October\\Rain\\Database\\Model->extendableCall(\'whereHas\', Array)\n#7 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Model.php(1489): October\\Rain\\Database\\Model->__call(\'whereHas\', Array)\n#8 /Applications/MAMP/htdocs/rhm/plugins/alipo/blog/components/Postcpdetail.php(27): Illuminate\\Database\\Eloquent\\Model::__callStatic(\'whereHas\', Array)\n#9 /Applications/MAMP/htdocs/rhm/modules/cms/Classes/CmsCompoundObject.php(168): Alipo\\Blog\\Components\\Postcpdetail->onRun()\n#10 /Applications/MAMP/htdocs/rhm/modules/cms/Classes/Controller.php(499): Cms\\Classes\\CmsCompoundObject->runComponents()\n#11 /Applications/MAMP/htdocs/rhm/modules/cms/Classes/Controller.php(392): Cms\\Classes\\Controller->execPageCycle()\n#12 /Applications/MAMP/htdocs/rhm/modules/cms/Classes/Controller.php(223): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#13 /Applications/MAMP/htdocs/rhm/modules/cms/Classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'tin-tuc/default\')\n#14 [internal function]: Cms\\Classes\\CmsController->run(\'tin-tuc/default\')\n#15 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#16 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#17 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#18 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#19 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#20 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#21 /Applications/MAMP/htdocs/rhm/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#22 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#23 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#24 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#25 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#26 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#27 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#28 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#29 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#30 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#31 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#32 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#33 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#35 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#36 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#41 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#42 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#43 /Applications/MAMP/htdocs/rhm/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#44 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#45 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#47 /Applications/MAMP/htdocs/rhm/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(24): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#48 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#49 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#50 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#51 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#52 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#53 /Applications/MAMP/htdocs/rhm/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#54 {main}', NULL, '2022-12-07 06:52:54', '2022-12-07 06:52:54'),
(3, 'error', 'Twig\\Error\\SyntaxError: Unexpected token \"end of statement block\" of value \"\" in \"/Applications/MAMP/htdocs/rhm/themes/custom/partials/header.htm\" at line 37. in /Applications/MAMP/htdocs/rhm/vendor/twig/twig/src/ExpressionParser.php:285\nStack trace:\n#0 /Applications/MAMP/htdocs/rhm/vendor/twig/twig/src/ExpressionParser.php(175): Twig\\ExpressionParser->parsePrimaryExpression()\n#1 /Applications/MAMP/htdocs/rhm/vendor/twig/twig/src/ExpressionParser.php(70): Twig\\ExpressionParser->getPrimary()\n#2 /Applications/MAMP/htdocs/rhm/vendor/twig/twig/src/TokenParser/IfTokenParser.php(52): Twig\\ExpressionParser->parseExpression()\n#3 /Applications/MAMP/htdocs/rhm/vendor/twig/twig/src/Parser.php(185): Twig\\TokenParser\\IfTokenParser->parse(Object(Twig\\Token))\n#4 /Applications/MAMP/htdocs/rhm/vendor/twig/twig/src/Parser.php(98): Twig\\Parser->subparse(NULL, false)\n#5 /Applications/MAMP/htdocs/rhm/vendor/twig/twig/src/Environment.php(563): Twig\\Parser->parse(Object(Twig\\TokenStream))\n#6 /Applications/MAMP/htdocs/rhm/vendor/twig/twig/src/Environment.php(595): Twig\\Environment->parse(Object(Twig\\TokenStream))\n#7 /Applications/MAMP/htdocs/rhm/vendor/twig/twig/src/Environment.php(408): Twig\\Environment->compileSource(Object(Twig\\Source))\n#8 /Applications/MAMP/htdocs/rhm/vendor/twig/twig/src/Environment.php(381): Twig\\Environment->loadClass(\'__TwigTemplate_...\', \'/Applications/M...\', NULL)\n#9 /Applications/MAMP/htdocs/rhm/modules/cms/Classes/Controller.php(1084): Twig\\Environment->loadTemplate(\'/Applications/M...\')\n#10 /Applications/MAMP/htdocs/rhm/modules/cms/Twig/Extension.php(102): Cms\\Classes\\Controller->renderPartial(\'header\', Array, true)\n#11 /Applications/MAMP/htdocs/rhm/storage/cms/twig/a3/a3424373acec0de9db9aa2c5b19a250818c9fc08c61fdc5ec76d0693c7e906b7.php(116): Cms\\Twig\\Extension->partialFunction(\'header\', Array, true)\n#12 /Applications/MAMP/htdocs/rhm/vendor/twig/twig/src/Template.php(407): __TwigTemplate_7597cfff220373e25f296c1a3d38ed79152a511e0766363a0da9570d9e6ed6c3->doDisplay(Array, Array)\n#13 /Applications/MAMP/htdocs/rhm/vendor/twig/twig/src/Template.php(380): Twig\\Template->displayWithErrorHandling(Array, Array)\n#14 /Applications/MAMP/htdocs/rhm/vendor/twig/twig/src/Template.php(392): Twig\\Template->display(Array)\n#15 /Applications/MAMP/htdocs/rhm/modules/cms/Classes/Controller.php(433): Twig\\Template->render(Array)\n#16 /Applications/MAMP/htdocs/rhm/modules/cms/Classes/Controller.php(223): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#17 /Applications/MAMP/htdocs/rhm/modules/cms/Classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'/\')\n#18 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#19 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#20 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#21 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#22 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#23 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#24 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#25 /Applications/MAMP/htdocs/rhm/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#26 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#27 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#28 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#29 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#30 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#31 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#32 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#33 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#35 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#36 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#38 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#39 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#41 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#42 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#43 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#44 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#45 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#46 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#47 /Applications/MAMP/htdocs/rhm/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#48 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#49 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#50 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#51 /Applications/MAMP/htdocs/rhm/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(24): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#52 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#53 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#54 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#55 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#56 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#57 /Applications/MAMP/htdocs/rhm/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#58 {main}', NULL, '2022-12-07 08:39:27', '2022-12-07 08:39:27'),
(4, 'error', 'Twig\\Error\\SyntaxError: Unexpected token \"end of statement block\" of value \"\" in \"/Applications/MAMP/htdocs/rhm/themes/custom/partials/header.htm\" at line 37. in /Applications/MAMP/htdocs/rhm/vendor/twig/twig/src/ExpressionParser.php:285\nStack trace:\n#0 /Applications/MAMP/htdocs/rhm/vendor/twig/twig/src/ExpressionParser.php(175): Twig\\ExpressionParser->parsePrimaryExpression()\n#1 /Applications/MAMP/htdocs/rhm/vendor/twig/twig/src/ExpressionParser.php(70): Twig\\ExpressionParser->getPrimary()\n#2 /Applications/MAMP/htdocs/rhm/vendor/twig/twig/src/TokenParser/IfTokenParser.php(52): Twig\\ExpressionParser->parseExpression()\n#3 /Applications/MAMP/htdocs/rhm/vendor/twig/twig/src/Parser.php(185): Twig\\TokenParser\\IfTokenParser->parse(Object(Twig\\Token))\n#4 /Applications/MAMP/htdocs/rhm/vendor/twig/twig/src/Parser.php(98): Twig\\Parser->subparse(NULL, false)\n#5 /Applications/MAMP/htdocs/rhm/vendor/twig/twig/src/Environment.php(563): Twig\\Parser->parse(Object(Twig\\TokenStream))\n#6 /Applications/MAMP/htdocs/rhm/vendor/twig/twig/src/Environment.php(595): Twig\\Environment->parse(Object(Twig\\TokenStream))\n#7 /Applications/MAMP/htdocs/rhm/vendor/twig/twig/src/Environment.php(408): Twig\\Environment->compileSource(Object(Twig\\Source))\n#8 /Applications/MAMP/htdocs/rhm/vendor/twig/twig/src/Environment.php(381): Twig\\Environment->loadClass(\'__TwigTemplate_...\', \'/Applications/M...\', NULL)\n#9 /Applications/MAMP/htdocs/rhm/modules/cms/Classes/Controller.php(1084): Twig\\Environment->loadTemplate(\'/Applications/M...\')\n#10 /Applications/MAMP/htdocs/rhm/modules/cms/Twig/Extension.php(102): Cms\\Classes\\Controller->renderPartial(\'header\', Array, true)\n#11 /Applications/MAMP/htdocs/rhm/storage/cms/twig/a3/a3424373acec0de9db9aa2c5b19a250818c9fc08c61fdc5ec76d0693c7e906b7.php(116): Cms\\Twig\\Extension->partialFunction(\'header\', Array, true)\n#12 /Applications/MAMP/htdocs/rhm/vendor/twig/twig/src/Template.php(407): __TwigTemplate_7597cfff220373e25f296c1a3d38ed79152a511e0766363a0da9570d9e6ed6c3->doDisplay(Array, Array)\n#13 /Applications/MAMP/htdocs/rhm/vendor/twig/twig/src/Template.php(380): Twig\\Template->displayWithErrorHandling(Array, Array)\n#14 /Applications/MAMP/htdocs/rhm/vendor/twig/twig/src/Template.php(392): Twig\\Template->display(Array)\n#15 /Applications/MAMP/htdocs/rhm/modules/cms/Classes/Controller.php(433): Twig\\Template->render(Array)\n#16 /Applications/MAMP/htdocs/rhm/modules/cms/Classes/Controller.php(223): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#17 /Applications/MAMP/htdocs/rhm/modules/cms/Classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'/\')\n#18 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#19 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#20 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#21 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#22 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#23 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#24 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#25 /Applications/MAMP/htdocs/rhm/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#26 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#27 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#28 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#29 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#30 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#31 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#32 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#33 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#35 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#36 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#37 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#38 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#39 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#40 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#41 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#42 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#43 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#44 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#45 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#46 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#47 /Applications/MAMP/htdocs/rhm/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#48 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#49 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#50 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#51 /Applications/MAMP/htdocs/rhm/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(24): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#52 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#53 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#54 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#55 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#56 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#57 /Applications/MAMP/htdocs/rhm/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#58 {main}', NULL, '2022-12-07 08:39:34', '2022-12-07 08:39:34');
INSERT INTO `system_event_logs` (`id`, `level`, `message`, `details`, `created_at`, `updated_at`) VALUES
(5, 'error', 'Twig\\Error\\SyntaxError: Unexpected character \"\'\" in \"/Applications/MAMP/htdocs/rhm/themes/custom/pages/tin-tuc-chi-tiet.htm\" at line 21. in /Applications/MAMP/htdocs/rhm/vendor/twig/twig/src/Lexer.php:365\nStack trace:\n#0 /Applications/MAMP/htdocs/rhm/vendor/twig/twig/src/Lexer.php(292): Twig\\Lexer->lexExpression()\n#1 /Applications/MAMP/htdocs/rhm/vendor/twig/twig/src/Lexer.php(186): Twig\\Lexer->lexVar()\n#2 /Applications/MAMP/htdocs/rhm/vendor/twig/twig/src/Environment.php(542): Twig\\Lexer->tokenize(Object(Twig\\Source))\n#3 /Applications/MAMP/htdocs/rhm/vendor/twig/twig/src/Environment.php(595): Twig\\Environment->tokenize(Object(Twig\\Source))\n#4 /Applications/MAMP/htdocs/rhm/vendor/twig/twig/src/Environment.php(408): Twig\\Environment->compileSource(Object(Twig\\Source))\n#5 /Applications/MAMP/htdocs/rhm/vendor/twig/twig/src/Environment.php(381): Twig\\Environment->loadClass(\'__TwigTemplate_...\', \'/Applications/M...\', NULL)\n#6 /Applications/MAMP/htdocs/rhm/modules/cms/Classes/Controller.php(422): Twig\\Environment->loadTemplate(\'/Applications/M...\')\n#7 /Applications/MAMP/htdocs/rhm/modules/cms/Classes/Controller.php(223): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#8 /Applications/MAMP/htdocs/rhm/modules/cms/Classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'tin-tuc/kham-va...\')\n#9 [internal function]: Cms\\Classes\\CmsController->run(\'tin-tuc/kham-va...\')\n#10 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#11 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#12 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#13 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#14 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#15 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#16 /Applications/MAMP/htdocs/rhm/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#17 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#18 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#19 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#20 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#21 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#22 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#23 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#24 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#25 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#26 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#27 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#28 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#29 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#30 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#31 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#32 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#33 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#35 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#36 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#37 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#38 /Applications/MAMP/htdocs/rhm/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#39 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#40 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#41 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#42 /Applications/MAMP/htdocs/rhm/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(24): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#43 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#44 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#47 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#48 /Applications/MAMP/htdocs/rhm/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#49 {main}', NULL, '2022-12-07 08:46:35', '2022-12-07 08:46:35'),
(6, 'error', 'Twig\\Error\\SyntaxError: Unexpected character \"\'\" in \"/Applications/MAMP/htdocs/rhm/themes/custom/pages/tin-tuc-chi-tiet.htm\" at line 21. in /Applications/MAMP/htdocs/rhm/vendor/twig/twig/src/Lexer.php:365\nStack trace:\n#0 /Applications/MAMP/htdocs/rhm/vendor/twig/twig/src/Lexer.php(292): Twig\\Lexer->lexExpression()\n#1 /Applications/MAMP/htdocs/rhm/vendor/twig/twig/src/Lexer.php(186): Twig\\Lexer->lexVar()\n#2 /Applications/MAMP/htdocs/rhm/vendor/twig/twig/src/Environment.php(542): Twig\\Lexer->tokenize(Object(Twig\\Source))\n#3 /Applications/MAMP/htdocs/rhm/vendor/twig/twig/src/Environment.php(595): Twig\\Environment->tokenize(Object(Twig\\Source))\n#4 /Applications/MAMP/htdocs/rhm/vendor/twig/twig/src/Environment.php(408): Twig\\Environment->compileSource(Object(Twig\\Source))\n#5 /Applications/MAMP/htdocs/rhm/vendor/twig/twig/src/Environment.php(381): Twig\\Environment->loadClass(\'__TwigTemplate_...\', \'/Applications/M...\', NULL)\n#6 /Applications/MAMP/htdocs/rhm/modules/cms/Classes/Controller.php(422): Twig\\Environment->loadTemplate(\'/Applications/M...\')\n#7 /Applications/MAMP/htdocs/rhm/modules/cms/Classes/Controller.php(223): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#8 /Applications/MAMP/htdocs/rhm/modules/cms/Classes/CmsController.php(50): Cms\\Classes\\Controller->run(\'tin-tuc/kham-va...\')\n#9 [internal function]: Cms\\Classes\\CmsController->run(\'tin-tuc/kham-va...\')\n#10 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array(Array, Array)\n#11 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#12 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#13 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Route.php(169): Illuminate\\Routing\\Route->runController()\n#14 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Router.php(658): Illuminate\\Routing\\Route->run()\n#15 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#16 /Applications/MAMP/htdocs/rhm/plugins/rainlab/translate/classes/LocaleMiddleware.php(29): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#17 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): RainLab\\Translate\\Classes\\LocaleMiddleware->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#18 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#19 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Middleware/SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#20 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#21 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#22 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/View/Middleware/ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#23 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#24 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#25 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Session/Middleware/StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#26 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#27 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#28 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#29 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#30 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#31 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Cookie/Middleware/EncryptCookies.php(66): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#32 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#33 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#34 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#35 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#36 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#37 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#38 /Applications/MAMP/htdocs/rhm/vendor/october/rain/src/Router/CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#39 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#40 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#41 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Foundation/Http/Middleware/CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#42 /Applications/MAMP/htdocs/rhm/vendor/october/rain/src/Foundation/Http/Middleware/CheckForMaintenanceMode.php(24): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#43 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(149): October\\Rain\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#44 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Routing/Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#45 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#46 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#47 /Applications/MAMP/htdocs/rhm/vendor/laravel/framework/src/Illuminate/Foundation/Http/Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#48 /Applications/MAMP/htdocs/rhm/index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#49 {main}', NULL, '2022-12-07 08:46:50', '2022-12-07 08:46:50');

-- --------------------------------------------------------

--
-- Table structure for table `system_files`
--

CREATE TABLE `system_files` (
  `id` int(10) UNSIGNED NOT NULL,
  `disk_name` varchar(191) NOT NULL,
  `file_name` varchar(191) NOT NULL,
  `file_size` int(11) NOT NULL,
  `content_type` varchar(191) NOT NULL,
  `title` varchar(191) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `field` varchar(191) DEFAULT NULL,
  `attachment_id` varchar(191) DEFAULT NULL,
  `attachment_type` varchar(191) DEFAULT NULL,
  `is_public` tinyint(1) NOT NULL DEFAULT 1,
  `sort_order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_files`
--

INSERT INTO `system_files` (`id`, `disk_name`, `file_name`, `file_size`, `content_type`, `title`, `description`, `field`, `attachment_id`, `attachment_type`, `is_public`, `sort_order`, `created_at`, `updated_at`) VALUES
(1, '6390adf66890a257268841.png', 'logoTVU_-AI-01-150x150.png', 25152, 'image/png', NULL, NULL, 'logo', '1', 'Alipo\\Cms\\Models\\General', 1, 1, '2022-12-07 08:15:02', '2022-12-07 08:16:06'),
(2, '6390b4929f248339944848.jpeg', 'cong-rhm-1024x603.jpeg', 164352, 'image/jpeg', NULL, NULL, 'thumb', '1', 'Alipo\\Blog\\Models\\Post', 1, 2, '2022-12-07 08:43:14', '2022-12-07 08:43:33'),
(3, '6390b4ea41a18524846294.jpeg', 'cong-rhm-1024x603.jpeg', 164352, 'image/jpeg', NULL, NULL, 'thumb', '2', 'Alipo\\Blog\\Models\\Post', 1, 3, '2022-12-07 08:44:42', '2022-12-07 08:45:03'),
(4, '63920dec827af376204403.jpg', 'Slider 1.jpg', 417073, 'image/jpeg', NULL, NULL, 'image', '1', 'Utopigs\\Seo\\Models\\Seo', 1, 4, '2022-12-08 09:16:44', '2022-12-08 09:16:53');

-- --------------------------------------------------------

--
-- Table structure for table `system_mail_layouts`
--

CREATE TABLE `system_mail_layouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) DEFAULT NULL,
  `code` varchar(191) DEFAULT NULL,
  `content_html` text DEFAULT NULL,
  `content_text` text DEFAULT NULL,
  `content_css` text DEFAULT NULL,
  `is_locked` tinyint(1) NOT NULL DEFAULT 0,
  `options` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_mail_layouts`
--

INSERT INTO `system_mail_layouts` (`id`, `name`, `code`, `content_html`, `content_text`, `content_css`, `is_locked`, `options`, `created_at`, `updated_at`) VALUES
(1, 'Default layout', 'default', '<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n</head>\n<body>\n    <style type=\"text/css\" media=\"screen\">\n        {{ brandCss|raw }}\n        {{ css|raw }}\n    </style>\n\n    <table class=\"wrapper layout-default\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n\n        <!-- Header -->\n        {% partial \'header\' body %}\n            {{ subject|raw }}\n        {% endpartial %}\n\n        <tr>\n            <td align=\"center\">\n                <table class=\"content\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                    <!-- Email Body -->\n                    <tr>\n                        <td class=\"body\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                            <table class=\"inner-body\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\">\n                                <!-- Body content -->\n                                <tr>\n                                    <td class=\"content-cell\">\n                                        {{ content|raw }}\n                                    </td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n                </table>\n            </td>\n        </tr>\n\n        <!-- Footer -->\n        {% partial \'footer\' body %}\n            &copy; {{ \"now\"|date(\"Y\") }} {{ appName }}. All rights reserved.\n        {% endpartial %}\n\n    </table>\n\n</body>\n</html>', '{{ content|raw }}', '@media only screen and (max-width: 600px) {\n    .inner-body {\n        width: 100% !important;\n    }\n\n    .footer {\n        width: 100% !important;\n    }\n}\n\n@media only screen and (max-width: 500px) {\n    .button {\n        width: 100% !important;\n    }\n}', 1, NULL, '2022-12-07 06:25:28', '2022-12-07 06:25:28'),
(2, 'System layout', 'system', '<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n</head>\n<body>\n    <style type=\"text/css\" media=\"screen\">\n        {{ brandCss|raw }}\n        {{ css|raw }}\n    </style>\n\n    <table class=\"wrapper layout-system\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n        <tr>\n            <td align=\"center\">\n                <table class=\"content\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                    <!-- Email Body -->\n                    <tr>\n                        <td class=\"body\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                            <table class=\"inner-body\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\">\n                                <!-- Body content -->\n                                <tr>\n                                    <td class=\"content-cell\">\n                                        {{ content|raw }}\n\n                                        <!-- Subcopy -->\n                                        {% partial \'subcopy\' body %}\n                                            **This is an automatic message. Please do not reply to it.**\n                                        {% endpartial %}\n                                    </td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n                </table>\n            </td>\n        </tr>\n    </table>\n\n</body>\n</html>', '{{ content|raw }}\n\n\n---\nThis is an automatic message. Please do not reply to it.', '@media only screen and (max-width: 600px) {\n    .inner-body {\n        width: 100% !important;\n    }\n\n    .footer {\n        width: 100% !important;\n    }\n}\n\n@media only screen and (max-width: 500px) {\n    .button {\n        width: 100% !important;\n    }\n}', 1, NULL, '2022-12-07 06:25:28', '2022-12-07 06:25:28');

-- --------------------------------------------------------

--
-- Table structure for table `system_mail_partials`
--

CREATE TABLE `system_mail_partials` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) DEFAULT NULL,
  `code` varchar(191) DEFAULT NULL,
  `content_html` text DEFAULT NULL,
  `content_text` text DEFAULT NULL,
  `is_custom` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_mail_templates`
--

CREATE TABLE `system_mail_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) DEFAULT NULL,
  `subject` varchar(191) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `content_html` text DEFAULT NULL,
  `content_text` text DEFAULT NULL,
  `layout_id` int(11) DEFAULT NULL,
  `is_custom` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_parameters`
--

CREATE TABLE `system_parameters` (
  `id` int(10) UNSIGNED NOT NULL,
  `namespace` varchar(100) NOT NULL,
  `group` varchar(50) NOT NULL,
  `item` varchar(150) NOT NULL,
  `value` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_parameters`
--

INSERT INTO `system_parameters` (`id`, `namespace`, `group`, `item`, `value`) VALUES
(1, 'system', 'update', 'count', '0'),
(2, 'cms', 'theme', 'active', '\"custom\"'),
(3, 'system', 'update', 'retry', '1670602332');

-- --------------------------------------------------------

--
-- Table structure for table `system_plugin_history`
--

CREATE TABLE `system_plugin_history` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) NOT NULL,
  `type` varchar(20) NOT NULL,
  `version` varchar(50) NOT NULL,
  `detail` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_plugin_history`
--

INSERT INTO `system_plugin_history` (`id`, `code`, `type`, `version`, `detail`, `created_at`) VALUES
(1, 'Alipo.About', 'script', '1.0.2', 'create_posts_table.php', '2022-12-07 06:25:27'),
(2, 'Alipo.About', 'comment', '1.0.2', 'First version of About', '2022-12-07 06:25:27'),
(3, 'Alipo.Announce', 'script', '1.1.5', 'create_posts_table.php', '2022-12-07 06:25:27'),
(4, 'Alipo.Announce', 'comment', '1.1.5', 'First version of Announce', '2022-12-07 06:25:27'),
(5, 'Alipo.Blog', 'script', '1.0.2', 'create_categories_table.php', '2022-12-07 06:25:27'),
(6, 'Alipo.Blog', 'script', '1.0.2', 'create_category_posts_table.php', '2022-12-07 06:25:27'),
(7, 'Alipo.Blog', 'script', '1.0.2', 'create_posts_table.php', '2022-12-07 06:25:27'),
(8, 'Alipo.Blog', 'comment', '1.0.2', 'First version of Blog', '2022-12-07 06:25:27'),
(9, 'Alipo.Cms', 'script', '1.1.6', 'create_aboutpages_table.php', '2022-12-07 06:25:27'),
(10, 'Alipo.Cms', 'script', '1.1.6', 'create_contactpages_table.php', '2022-12-07 06:25:27'),
(11, 'Alipo.Cms', 'script', '1.1.6', 'create_faqpages_table.php', '2022-12-07 06:25:27'),
(12, 'Alipo.Cms', 'script', '1.1.6', 'create_homepages_table.php', '2022-12-07 06:25:27'),
(13, 'Alipo.Cms', 'script', '1.1.6', 'create_newspages_table.php', '2022-12-07 06:25:27'),
(14, 'Alipo.Cms', 'script', '1.1.6', 'create_servicepages_table.php', '2022-12-07 06:25:27'),
(15, 'Alipo.Cms', 'script', '1.1.6', 'create_teampages_table.php', '2022-12-07 06:25:27'),
(16, 'Alipo.Cms', 'script', '1.1.6', 'create_generals_table.php', '2022-12-07 06:25:27'),
(17, 'Alipo.Cms', 'script', '1.1.6', 'create_contact_infos_table.php', '2022-12-07 06:25:27'),
(18, 'Alipo.Cms', 'script', '1.1.6', 'create_nckhs_table.php', '2022-12-07 06:25:27'),
(19, 'Alipo.Cms', 'script', '1.1.6', 'create_htqts_table.php', '2022-12-07 06:25:27'),
(20, 'Alipo.Cms', 'script', '1.1.6', 'create_students_table.php', '2022-12-07 06:25:27'),
(21, 'Alipo.Cms', 'comment', '1.1.6', 'First version of Cms', '2022-12-07 06:25:27'),
(22, 'Alipo.Subject', 'script', '1.0.1', 'create_posts_table.php', '2022-12-07 06:25:27'),
(23, 'Alipo.Subject', 'comment', '1.0.1', 'First version of Subject', '2022-12-07 06:25:27'),
(24, 'Alipo.Training', 'script', '1.0.1', 'create_posts_table.php', '2022-12-07 06:25:27'),
(25, 'Alipo.Training', 'comment', '1.0.1', 'First version of Training', '2022-12-07 06:25:27'),
(26, 'AnandPatel.WysiwygEditors', 'comment', '1.0.1', 'First version of Wysiwyg Editors.', '2022-12-07 06:25:27'),
(27, 'AnandPatel.WysiwygEditors', 'comment', '1.0.2', 'Automatic Injection to CMS & other code editors and October CMS`s Rich Editor added.', '2022-12-07 06:25:27'),
(28, 'AnandPatel.WysiwygEditors', 'comment', '1.0.3', 'Automatic Injection to RainLab Static Pages & other plugin`s option is appear only if installed.', '2022-12-07 06:25:27'),
(29, 'AnandPatel.WysiwygEditors', 'comment', '1.0.4', 'New Froala editor added (on request from emzero439), Height & width property added for editor, setting moved to My Setting tab and minor changes in settings.', '2022-12-07 06:25:27'),
(30, 'AnandPatel.WysiwygEditors', 'comment', '1.0.5', 'Automatic Injection to Radiantweb`s Problog and ProEvents (option available in settings-content).', '2022-12-07 06:25:27'),
(31, 'AnandPatel.WysiwygEditors', 'comment', '1.0.6', 'CKEditor updated and bug fixes.', '2022-12-07 06:25:27'),
(32, 'AnandPatel.WysiwygEditors', 'comment', '1.0.7', 'Integrated elFinder (file browser) with TinyMCE & CKEditor, Image upload/delete for Froala Editor.', '2022-12-07 06:25:27'),
(33, 'AnandPatel.WysiwygEditors', 'comment', '1.0.8', 'Added security to File Browser`s route (Authenticate users can only access File Browser).', '2022-12-07 06:25:27'),
(34, 'AnandPatel.WysiwygEditors', 'comment', '1.0.9', 'Updated CKEditor, Froala and TinyMCE.', '2022-12-07 06:25:27'),
(35, 'AnandPatel.WysiwygEditors', 'comment', '1.1.0', 'Support multilanguage, update elFinder and cleanup code.', '2022-12-07 06:25:27'),
(36, 'AnandPatel.WysiwygEditors', 'comment', '1.1.1', 'Added Turkish language.', '2022-12-07 06:25:27'),
(37, 'AnandPatel.WysiwygEditors', 'comment', '1.1.2', 'Added Hungarian language.', '2022-12-07 06:25:27'),
(38, 'AnandPatel.WysiwygEditors', 'comment', '1.1.3', 'Fixed issue related to RC (Elfinder fix remaining).', '2022-12-07 06:25:27'),
(39, 'AnandPatel.WysiwygEditors', 'comment', '1.1.4', 'Fixed Elfinder issue.', '2022-12-07 06:25:27'),
(40, 'AnandPatel.WysiwygEditors', 'comment', '1.1.5', 'Updated CKEditor, Froala and TinyMCE.', '2022-12-07 06:25:27'),
(41, 'AnandPatel.WysiwygEditors', 'comment', '1.1.6', 'Changed destination folder.', '2022-12-07 06:25:27'),
(42, 'AnandPatel.WysiwygEditors', 'comment', '1.1.7', 'Added Czech language.', '2022-12-07 06:25:27'),
(43, 'AnandPatel.WysiwygEditors', 'comment', '1.1.8', 'Added Russian language.', '2022-12-07 06:25:27'),
(44, 'AnandPatel.WysiwygEditors', 'comment', '1.1.9', 'Fix hook and other issues (thanks to Vojta Svoboda).', '2022-12-07 06:25:27'),
(45, 'AnandPatel.WysiwygEditors', 'comment', '1.2.0', 'Put settings inside CMS sidemenu (thanks to Amanda Tresbach).', '2022-12-07 06:25:27'),
(46, 'AnandPatel.WysiwygEditors', 'comment', '1.2.1', 'Remove el-finder (Which fix issue of composer), added OC media manager support for image in TinyMCE & CkEditor, TinyMCE version updated, Fix Image upload for froala editor', '2022-12-07 06:25:27'),
(47, 'AnandPatel.WysiwygEditors', 'comment', '1.2.2', 'Add multilingual support, Add new languages, Update the Hungarian language, Add the missing English language files (Special thanks to Szabó Gergő)', '2022-12-07 06:25:27'),
(48, 'AnandPatel.WysiwygEditors', 'comment', '1.2.3', 'Added toolbar customization option (Special thanks to Szabó Gergő).', '2022-12-07 06:25:27'),
(49, 'AnandPatel.WysiwygEditors', 'comment', '1.2.4', 'Added support for Content Plus Plugin & News and Newsletter plugin (thanks to Szabó Gergő)', '2022-12-07 06:25:27'),
(50, 'AnandPatel.WysiwygEditors', 'comment', '1.2.5', 'Minor improvements and bugfixes.', '2022-12-07 06:25:27'),
(51, 'AnandPatel.WysiwygEditors', 'comment', '1.2.6', 'Updated the CKEditor and TinyMCE editors.', '2022-12-07 06:25:27'),
(52, 'AnandPatel.WysiwygEditors', 'comment', '1.2.7', 'Show locale switcher when using multilocale editor.', '2022-12-07 06:25:27'),
(53, 'AnandPatel.WysiwygEditors', 'comment', '1.2.8', 'Added French language', '2022-12-07 06:25:27'),
(54, 'AnandPatel.WysiwygEditors', 'comment', '1.2.9', 'Added permission for preferences', '2022-12-07 06:25:27'),
(55, 'BenFreke.MenuManager', 'script', '1.0.1', 'create_menus_table.php', '2022-12-07 06:25:27'),
(56, 'BenFreke.MenuManager', 'comment', '1.0.1', 'First version of MenuManager', '2022-12-07 06:25:27'),
(57, 'BenFreke.MenuManager', 'comment', '1.0.2', 'Added active state to menu; Added ability to select active menu item; Added controllable depth to component', '2022-12-07 06:25:27'),
(58, 'BenFreke.MenuManager', 'comment', '1.0.3', 'Made it possible for menu items to not link anywhere; Put a check in so the active node must be a child of the parentNode', '2022-12-07 06:25:27'),
(59, 'BenFreke.MenuManager', 'comment', '1.0.4', 'Fixed bug where re-ordering stopped working', '2022-12-07 06:25:27'),
(60, 'BenFreke.MenuManager', 'comment', '1.0.5', 'Moved link creation code into the model in preparation for external links; Brought list item class creation into the model; Fixed typo with default menu class', '2022-12-07 06:25:27'),
(61, 'BenFreke.MenuManager', 'comment', '1.0.6', 'Removed NestedSetModel, thanks @daftspunk', '2022-12-07 06:25:27'),
(62, 'BenFreke.MenuManager', 'script', '1.1.0', 'add_is_external_field.php', '2022-12-07 06:25:27'),
(63, 'BenFreke.MenuManager', 'script', '1.1.0', 'add_link_target_field.php', '2022-12-07 06:25:27'),
(64, 'BenFreke.MenuManager', 'comment', '1.1.0', 'Added ability to link to external sites. Thanks @adisos', '2022-12-07 06:25:27'),
(65, 'BenFreke.MenuManager', 'script', '1.1.1', 'add_enabled_parameters_query_fields_1.php', '2022-12-07 06:25:27'),
(66, 'BenFreke.MenuManager', 'script', '1.1.1', 'add_enabled_parameters_query_fields_2.php', '2022-12-07 06:25:28'),
(67, 'BenFreke.MenuManager', 'script', '1.1.1', 'add_enabled_parameters_query_fields_3.php', '2022-12-07 06:25:28'),
(68, 'BenFreke.MenuManager', 'comment', '1.1.1', 'Added ability to enable/disable individual menu links; Added ability for url parameters & query string; Fixed issue of \"getLinkHref()\" pulling through full page url with parameters rather than the ACTUAL page url', '2022-12-07 06:25:28'),
(69, 'BenFreke.MenuManager', 'comment', '1.1.2', 'Reformatted code for better maintainability and better practises', '2022-12-07 06:25:28'),
(70, 'BenFreke.MenuManager', 'comment', '1.1.3', 'Fixed bug that prevented multiple components displaying', '2022-12-07 06:25:28'),
(71, 'BenFreke.MenuManager', 'comment', '1.2.0', 'Fixed validation and fields bug; Added list classes', '2022-12-07 06:25:28'),
(72, 'BenFreke.MenuManager', 'comment', '1.3.0', 'Added translation ability thanks to @alxy', '2022-12-07 06:25:28'),
(73, 'BenFreke.MenuManager', 'comment', '1.3.1', 'JSON validation of parameters added; Correct active menu now being set based on parameters; PR sent by @whsol, thanks!', '2022-12-07 06:25:28'),
(74, 'BenFreke.MenuManager', 'comment', '1.3.2', 'Fix for param check that is breaking active node lookup. Thanks @alxy', '2022-12-07 06:25:28'),
(75, 'BenFreke.MenuManager', 'comment', '1.3.3', 'Fix for JSON comment having single quotes. Thanks @adisos', '2022-12-07 06:25:28'),
(76, 'BenFreke.MenuManager', 'comment', '1.3.4', 'Fix for JSON validation not firing', '2022-12-07 06:25:28'),
(77, 'BenFreke.MenuManager', 'script', '1.4.0', 'fix_menu_table.php', '2022-12-07 06:25:28'),
(78, 'BenFreke.MenuManager', 'comment', '1.4.0', 'Fix for POST operations. PR by @matissjanis, TR translations (@mahony0) and permission registration (@nnmer)', '2022-12-07 06:25:28'),
(79, 'BenFreke.MenuManager', 'comment', '1.4.1', 'Fixed bug caused by deleting needed method of Menu class. Thanks @MatissJA', '2022-12-07 06:25:28'),
(80, 'BenFreke.MenuManager', 'comment', '1.4.2', 'Fixed bug with URLs not saving correctly', '2022-12-07 06:25:28'),
(81, 'BenFreke.MenuManager', 'comment', '1.4.3', 'Fixed bug where getBaseFileName method was moved to a different object', '2022-12-07 06:25:28'),
(82, 'BenFreke.MenuManager', 'comment', '1.4.4', 'Fixed bug with incorrect labels. Thanks @ribsousa', '2022-12-07 06:25:28'),
(83, 'BenFreke.MenuManager', 'comment', '1.4.5', 'Fixed bug where getBaseFileName method was moved to a different object', '2022-12-07 06:25:28'),
(84, 'BenFreke.MenuManager', 'comment', '1.4.6', 'Merged PRs that fix bug with plugin not working with stable release', '2022-12-07 06:25:28'),
(85, 'BenFreke.MenuManager', 'comment', '1.4.7', 'Merged PR to fix syntax errors with fresh install of 1.4.6. Thanks @devlifeX', '2022-12-07 06:25:28'),
(86, 'BenFreke.MenuManager', 'comment', '1.4.8', 'Merged PR to fix re-order errors. Thanks @CptMeatball', '2022-12-07 06:25:28'),
(87, 'BenFreke.MenuManager', 'comment', '1.5.0', 'Fixed bugs preventing postgres and sqlite support', '2022-12-07 06:25:28'),
(88, 'BenFreke.MenuManager', 'comment', '1.5.1', 'Added homepage to plugin information. Thanks @gergo85', '2022-12-07 06:25:28'),
(89, 'BenFreke.MenuManager', 'comment', '1.5.2', 'Added French translation. Thanks @Glitchbone', '2022-12-07 06:25:28'),
(90, 'BenFreke.MenuManager', 'comment', '1.5.2', 'Added code of conduct', '2022-12-07 06:25:28'),
(91, 'BenFreke.MenuManager', 'comment', '1.5.3', 'Added ability to delete menus. Thanks @osmanzeki', '2022-12-07 06:25:28'),
(92, 'LaminSanneh.FlexiContact', 'comment', '1.0.0', 'First Version with basic functionality', '2022-12-07 06:25:28'),
(93, 'LaminSanneh.FlexiContact', 'comment', '1.0.1', 'Fixed email subject to send actual subject set in backend', '2022-12-07 06:25:28'),
(94, 'LaminSanneh.FlexiContact', 'comment', '1.0.2', 'Added validation to contact form fields', '2022-12-07 06:25:28'),
(95, 'LaminSanneh.FlexiContact', 'comment', '1.0.3', 'Changed body input field type from text to textarea', '2022-12-07 06:25:28'),
(96, 'LaminSanneh.FlexiContact', 'comment', '1.0.4', 'Updated default component markup to use more appropriate looking twitter bootstrap classes', '2022-12-07 06:25:28'),
(97, 'LaminSanneh.FlexiContact', 'comment', '1.0.5', 'Corrected spelling for Marketing on the backend settings', '2022-12-07 06:25:28'),
(98, 'LaminSanneh.FlexiContact', 'comment', '1.0.6', 'Added ability to include bootstrap or not on component config', '2022-12-07 06:25:28'),
(99, 'LaminSanneh.FlexiContact', 'comment', '1.0.6', 'Add proper validation message outputting', '2022-12-07 06:25:28'),
(100, 'LaminSanneh.FlexiContact', 'comment', '1.0.6', 'Added option to include or exclude main script file', '2022-12-07 06:25:28'),
(101, 'LaminSanneh.FlexiContact', 'comment', '1.0.7', 'Updated contact component default markup file', '2022-12-07 06:25:28'),
(102, 'LaminSanneh.FlexiContact', 'comment', '1.0.7', 'Updated readme file', '2022-12-07 06:25:28'),
(103, 'LaminSanneh.FlexiContact', 'comment', '1.1.0', 'Mail template is now registered properly', '2022-12-07 06:25:28'),
(104, 'LaminSanneh.FlexiContact', 'comment', '1.2.0', 'Add Proper validation that can be localized', '2022-12-07 06:25:28'),
(105, 'LaminSanneh.FlexiContact', 'comment', '1.2.1', 'Added permissions to the settings page. PR by @matissjanis', '2022-12-07 06:25:28'),
(106, 'LaminSanneh.FlexiContact', 'comment', '1.2.2', 'Added polish language features', '2022-12-07 06:25:28'),
(107, 'LaminSanneh.FlexiContact', 'comment', '1.2.3', 'Modified mail templatedefault text', '2022-12-07 06:25:28'),
(108, 'LaminSanneh.FlexiContact', 'comment', '1.3.0', '!!! Added captcha feature, which requires valid google captcha credentials to work', '2022-12-07 06:25:28'),
(109, 'LaminSanneh.FlexiContact', 'comment', '1.3.1', 'Set replyTo instead of from-header when sending', '2022-12-07 06:25:28'),
(110, 'LaminSanneh.FlexiContact', 'comment', '1.3.2', 'Added german translation language file', '2022-12-07 06:25:28'),
(111, 'LaminSanneh.FlexiContact', 'comment', '1.3.3', 'Added option to allow user to enable or disable captcha in contact form', '2022-12-07 06:25:28'),
(112, 'LaminSanneh.FlexiContact', 'comment', '1.3.4', 'Made sure captcha enabling or disabling doesnt produce bug', '2022-12-07 06:25:28'),
(113, 'October.Drivers', 'comment', '1.0.1', 'First version of Drivers', '2022-12-07 06:25:28'),
(114, 'October.Drivers', 'comment', '1.0.2', 'Update Guzzle library to version 5.0', '2022-12-07 06:25:28'),
(115, 'October.Drivers', 'comment', '1.1.0', 'Update AWS library to version 3.0', '2022-12-07 06:25:28'),
(116, 'October.Drivers', 'comment', '1.1.1', 'Update Guzzle library to version 6.0', '2022-12-07 06:25:28'),
(117, 'October.Drivers', 'comment', '1.1.2', 'Update Guzzle library to version 6.3', '2022-12-07 06:25:28'),
(118, 'RainLab.MailChimp', 'comment', '1.0.1', 'Initialize plugin', '2022-12-07 06:25:28'),
(119, 'RainLab.MailChimp', 'comment', '1.0.2', 'Minor improvements to the code', '2022-12-07 06:25:28'),
(120, 'RainLab.MailChimp', 'comment', '1.0.3', 'Improve compatibility with PHP7', '2022-12-07 06:25:28'),
(121, 'RainLab.MailChimp', 'comment', '1.0.4', 'Switch to MailChimp API v3', '2022-12-07 06:25:28'),
(122, 'RainLab.Translate', 'script', '1.0.1', 'create_messages_table.php', '2022-12-07 06:25:28'),
(123, 'RainLab.Translate', 'script', '1.0.1', 'create_attributes_table.php', '2022-12-07 06:25:28'),
(124, 'RainLab.Translate', 'script', '1.0.1', 'create_locales_table.php', '2022-12-07 06:25:28'),
(125, 'RainLab.Translate', 'comment', '1.0.1', 'First version of Translate', '2022-12-07 06:25:28'),
(126, 'RainLab.Translate', 'comment', '1.0.2', 'Languages and Messages can now be deleted.', '2022-12-07 06:25:28'),
(127, 'RainLab.Translate', 'comment', '1.0.3', 'Minor updates for latest October release.', '2022-12-07 06:25:28'),
(128, 'RainLab.Translate', 'comment', '1.0.4', 'Locale cache will clear when updating a language.', '2022-12-07 06:25:28'),
(129, 'RainLab.Translate', 'comment', '1.0.5', 'Add Spanish language and fix plugin config.', '2022-12-07 06:25:28'),
(130, 'RainLab.Translate', 'comment', '1.0.6', 'Minor improvements to the code.', '2022-12-07 06:25:28'),
(131, 'RainLab.Translate', 'comment', '1.0.7', 'Fixes major bug where translations are skipped entirely!', '2022-12-07 06:25:28'),
(132, 'RainLab.Translate', 'comment', '1.0.8', 'Minor bug fixes.', '2022-12-07 06:25:28'),
(133, 'RainLab.Translate', 'comment', '1.0.9', 'Fixes an issue where newly created models lose their translated values.', '2022-12-07 06:25:28'),
(134, 'RainLab.Translate', 'comment', '1.0.10', 'Minor fix for latest build.', '2022-12-07 06:25:28'),
(135, 'RainLab.Translate', 'comment', '1.0.11', 'Fix multilingual rich editor when used in stretch mode.', '2022-12-07 06:25:28'),
(136, 'RainLab.Translate', 'comment', '1.1.0', 'Introduce compatibility with RainLab.Pages plugin.', '2022-12-07 06:25:28'),
(137, 'RainLab.Translate', 'comment', '1.1.1', 'Minor UI fix to the language picker.', '2022-12-07 06:25:28'),
(138, 'RainLab.Translate', 'comment', '1.1.2', 'Add support for translating Static Content files.', '2022-12-07 06:25:28'),
(139, 'RainLab.Translate', 'comment', '1.1.3', 'Improved support for the multilingual rich editor.', '2022-12-07 06:25:28'),
(140, 'RainLab.Translate', 'comment', '1.1.4', 'Adds new multilingual markdown editor.', '2022-12-07 06:25:28'),
(141, 'RainLab.Translate', 'comment', '1.1.5', 'Minor update to the multilingual control API.', '2022-12-07 06:25:28'),
(142, 'RainLab.Translate', 'comment', '1.1.6', 'Minor improvements in the message editor.', '2022-12-07 06:25:28'),
(143, 'RainLab.Translate', 'comment', '1.1.7', 'Fixes bug not showing content when first loading multilingual textarea controls.', '2022-12-07 06:25:28'),
(144, 'RainLab.Translate', 'comment', '1.2.0', 'CMS pages now support translating the URL.', '2022-12-07 06:25:28'),
(145, 'RainLab.Translate', 'comment', '1.2.1', 'Minor update in the rich editor and code editor language control position.', '2022-12-07 06:25:28'),
(146, 'RainLab.Translate', 'comment', '1.2.2', 'Static Pages now support translating the URL.', '2022-12-07 06:25:28'),
(147, 'RainLab.Translate', 'comment', '1.2.3', 'Fixes Rich Editor when inserting a page link.', '2022-12-07 06:25:28'),
(148, 'RainLab.Translate', 'script', '1.2.4', 'create_indexes_table.php', '2022-12-07 06:25:28'),
(149, 'RainLab.Translate', 'comment', '1.2.4', 'Translatable attributes can now be declared as indexes.', '2022-12-07 06:25:28'),
(150, 'RainLab.Translate', 'comment', '1.2.5', 'Adds new multilingual repeater form widget.', '2022-12-07 06:25:28'),
(151, 'RainLab.Translate', 'comment', '1.2.6', 'Fixes repeater usage with static pages plugin.', '2022-12-07 06:25:28'),
(152, 'RainLab.Translate', 'comment', '1.2.7', 'Fixes placeholder usage with static pages plugin.', '2022-12-07 06:25:28'),
(153, 'RainLab.Translate', 'comment', '1.2.8', 'Improvements to code for latest October build compatibility.', '2022-12-07 06:25:28'),
(154, 'RainLab.Translate', 'comment', '1.2.9', 'Fixes context for translated strings when used with Static Pages.', '2022-12-07 06:25:28'),
(155, 'RainLab.Translate', 'comment', '1.2.10', 'Minor UI fix to the multilingual repeater.', '2022-12-07 06:25:28'),
(156, 'RainLab.Translate', 'comment', '1.2.11', 'Fixes translation not working with partials loaded via AJAX.', '2022-12-07 06:25:28'),
(157, 'RainLab.Translate', 'comment', '1.2.12', 'Add support for translating the new grouped repeater feature.', '2022-12-07 06:25:28'),
(158, 'RainLab.Translate', 'comment', '1.3.0', 'Added search to the translate messages page.', '2022-12-07 06:25:28'),
(159, 'RainLab.Translate', 'script', '1.3.1', 'builder_table_update_rainlab_translate_locales.php', '2022-12-07 06:25:28'),
(160, 'RainLab.Translate', 'script', '1.3.1', 'seed_all_tables.php', '2022-12-07 06:25:28'),
(161, 'RainLab.Translate', 'comment', '1.3.1', 'Added reordering to languages', '2022-12-07 06:25:28'),
(162, 'RainLab.Translate', 'comment', '1.3.2', 'Improved compatibility with RainLab.Pages, added ability to scan Mail Messages for translatable variables.', '2022-12-07 06:25:28'),
(163, 'RainLab.Translate', 'comment', '1.3.3', 'Fix to the locale picker session handling in Build 420 onwards.', '2022-12-07 06:25:28'),
(164, 'RainLab.Translate', 'comment', '1.3.4', 'Add alternate hreflang elements and adds prefixDefaultLocale setting.', '2022-12-07 06:25:28'),
(165, 'RainLab.Translate', 'comment', '1.3.5', 'Fix MLRepeater bug when switching locales.', '2022-12-07 06:25:28'),
(166, 'RainLab.Translate', 'comment', '1.3.6', 'Fix Middleware to use the prefixDefaultLocale setting introduced in 1.3.4', '2022-12-07 06:25:28'),
(167, 'RainLab.Translate', 'comment', '1.3.7', 'Fix config reference in LocaleMiddleware', '2022-12-07 06:25:28'),
(168, 'RainLab.Translate', 'comment', '1.3.8', 'Keep query string when switching locales', '2022-12-07 06:25:28'),
(169, 'RainLab.Translate', 'comment', '1.4.0', 'Add importer and exporter for messages', '2022-12-07 06:25:28'),
(170, 'RainLab.Translate', 'comment', '1.4.1', 'Updated Hungarian translation. Added Arabic translation. Fixed issue where default texts are overwritten by import. Fixed issue where the language switcher for repeater fields would overlap with the first repeater row.', '2022-12-07 06:25:28'),
(171, 'RainLab.Translate', 'comment', '1.4.2', 'Add multilingual MediaFinder', '2022-12-07 06:25:28'),
(172, 'RainLab.Translate', 'comment', '1.4.3', '!!! Please update OctoberCMS to Build 444 before updating this plugin. Added ability to translate CMS Pages fields (e.g. title, description, meta-title, meta-description)', '2022-12-07 06:25:28'),
(173, 'RainLab.Translate', 'comment', '1.4.4', 'Minor improvements to compatibility with Laravel framework.', '2022-12-07 06:25:28'),
(174, 'RainLab.Translate', 'comment', '1.4.5', 'Fixed issue when using the language switcher', '2022-12-07 06:25:28'),
(175, 'RainLab.Translate', 'comment', '1.5.0', 'Compatibility fix with Build 451', '2022-12-07 06:25:28'),
(176, 'RainLab.Translate', 'comment', '1.6.0', 'Make File Upload widget properties translatable. Merge Repeater core changes into MLRepeater widget. Add getter method to retrieve original translate data.', '2022-12-07 06:25:28'),
(177, 'RainLab.Translate', 'comment', '1.6.1', 'Add ability for models to provide translated computed data, add option to disable locale prefix routing', '2022-12-07 06:25:28'),
(178, 'RainLab.Translate', 'comment', '1.6.2', 'Implement localeUrl filter, add per-locale theme configuration support', '2022-12-07 06:25:28'),
(179, 'RainLab.Translate', 'comment', '1.6.3', 'Add eager loading for translations, restore support for accessors & mutators', '2022-12-07 06:25:28'),
(180, 'RainLab.Translate', 'comment', '1.6.4', 'Fixes PHP 7.4 compatibility', '2022-12-07 06:25:28'),
(181, 'RainLab.Translate', 'comment', '1.6.5', 'Fixes compatibility issue when other plugins use a custom model morph map', '2022-12-07 06:25:28'),
(182, 'RainLab.Translate', 'script', '1.6.6', 'migrate_morphed_attributes.php', '2022-12-07 06:25:28'),
(183, 'RainLab.Translate', 'comment', '1.6.6', 'Introduce migration to patch existing translations using morph map', '2022-12-07 06:25:28'),
(184, 'RainLab.Translate', 'script', '1.6.7', 'migrate_morphed_indexes.php', '2022-12-07 06:25:28'),
(185, 'RainLab.Translate', 'comment', '1.6.7', 'Introduce migration to patch existing indexes using morph map', '2022-12-07 06:25:28'),
(186, 'Utopigs.Seo', 'script', '1.0.1', 'create_data_table.php', '2022-12-07 06:25:28'),
(187, 'Utopigs.Seo', 'comment', '1.0.1', 'First version of Utopigs Seo plugin', '2022-12-07 06:25:28'),
(188, 'Utopigs.Seo', 'comment', '1.0.2', 'Fix bug with elements with nested items', '2022-12-07 06:25:28'),
(189, 'Utopigs.Seo', 'script', '1.1.0', 'create_sitemaps_table.php', '2022-12-07 06:25:28'),
(190, 'Utopigs.Seo', 'comment', '1.1.0', 'Add sitemap functionality', '2022-12-07 06:25:28'),
(191, 'Utopigs.Seo', 'comment', '1.1.0', 'Fix some bugs with nested items', '2022-12-07 06:25:28'),
(192, 'Utopigs.Seo', 'comment', '1.1.1', 'Fix browser render issue', '2022-12-07 06:25:28'),
(193, 'Utopigs.Seo', 'comment', '1.1.2', 'Undo last change to fix Google Search Console sitemap fetch issues', '2022-12-07 06:25:28'),
(194, 'Utopigs.Seo', 'comment', '1.1.3', 'Add a sitemap-debug.xml that renders ok (using https protocol for the namespace attributes) to be able to debug sitemap issues visually', '2022-12-07 06:25:28'),
(195, 'Utopigs.Seo', 'comment', '1.1.4', 'Fix bug model image not showing', '2022-12-07 06:25:28'),
(196, 'Utopigs.Seo', 'comment', '1.1.5', 'Fix bug with nested categories', '2022-12-07 06:25:28'),
(197, 'Utopigs.Seo', 'comment', '1.1.6', 'Try to retrieve image from defaults if not specified', '2022-12-07 06:25:28'),
(198, 'Utopigs.Seo', 'comment', '1.1.6', 'Autofill properties for blog post and category pages', '2022-12-07 06:25:28');

-- --------------------------------------------------------

--
-- Table structure for table `system_plugin_versions`
--

CREATE TABLE `system_plugin_versions` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) NOT NULL,
  `version` varchar(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `is_disabled` tinyint(1) NOT NULL DEFAULT 0,
  `is_frozen` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_plugin_versions`
--

INSERT INTO `system_plugin_versions` (`id`, `code`, `version`, `created_at`, `is_disabled`, `is_frozen`) VALUES
(1, 'Alipo.About', '1.0.2', '2022-12-07 06:25:27', 0, 0),
(2, 'Alipo.Announce', '1.1.5', '2022-12-07 06:25:27', 0, 0),
(3, 'Alipo.Blog', '1.0.2', '2022-12-07 06:25:27', 0, 0),
(4, 'Alipo.Cms', '1.1.6', '2022-12-07 06:25:27', 0, 0),
(5, 'Alipo.Subject', '1.0.1', '2022-12-07 06:25:27', 0, 0),
(6, 'Alipo.Training', '1.0.1', '2022-12-07 06:25:27', 0, 0),
(7, 'AnandPatel.WysiwygEditors', '1.2.9', '2022-12-07 06:25:27', 0, 0),
(8, 'BenFreke.MenuManager', '1.5.3', '2022-12-07 06:25:28', 0, 0),
(9, 'LaminSanneh.FlexiContact', '1.3.4', '2022-12-07 06:25:28', 0, 0),
(10, 'October.Drivers', '1.1.2', '2022-12-07 06:25:28', 0, 0),
(11, 'RainLab.MailChimp', '1.0.4', '2022-12-07 06:25:28', 0, 0),
(12, 'RainLab.Translate', '1.6.7', '2022-12-07 06:25:28', 0, 0),
(13, 'Utopigs.Seo', '1.1.6', '2022-12-07 06:25:28', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `system_request_logs`
--

CREATE TABLE `system_request_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `status_code` int(11) DEFAULT NULL,
  `url` varchar(191) DEFAULT NULL,
  `referer` text DEFAULT NULL,
  `count` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_revisions`
--

CREATE TABLE `system_revisions` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `field` varchar(191) DEFAULT NULL,
  `cast` varchar(191) DEFAULT NULL,
  `old_value` text DEFAULT NULL,
  `new_value` text DEFAULT NULL,
  `revisionable_type` varchar(191) NOT NULL,
  `revisionable_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_settings`
--

CREATE TABLE `system_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `item` varchar(191) DEFAULT NULL,
  `value` mediumtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_settings`
--

INSERT INTO `system_settings` (`id`, `item`, `value`) VALUES
(1, 'anandpatel_wysiwygeditors_settings', '{\"editor\":\"tinymce\",\"editor_width\":\"\",\"editor_height\":\"\",\"toolbar_tinymce\":\"undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media ocmediamanager\",\"toolbar_ckeditor\":\"[\'Undo\', \'Redo\'], [\'Cut\', \'Copy\', \'Paste\', \'PasteText\', \'PasteFromWord\'], [\'Format\', \'FontSize\'], [\'ShowBlocks\', \'SelectAll\', \'RemoveFormat\'], [\'Source\'], [\'Maximize\'], \'\\/\', [\'Bold\', \'Italic\', \'Underline\', \'Strike\'], [\'JustifyLeft\', \'JustifyCenter\', \'JustifyRight\', \'JustifyBlock\'], [\'BulletedList\', \'NumberedList\', \'Outdent\', \'Indent\'], [\'TextColor\', \'BGColor\'], [\'Link\', \'Unlink\', \'Anchor\'], [\'Image\', \'Table\', \'oembed\', \'SpecialChar\', \'OcMediaManager\']\",\"cms_page_as_wysiwyg\":\"0\",\"cms_content_as_wysiwyg\":\"0\",\"cms_partial_as_wysiwyg\":\"0\",\"cms_layout_as_wysiwyg\":\"0\",\"others_as_wysiwyg\":\"0\"}'),
(2, 'backend_brand_settings', '{\"app_name\":\"OctoberCMS\",\"app_tagline\":\"Getting back to basics\",\"primary_color\":\"#34495e\",\"secondary_color\":\"#e67e22\",\"accent_color\":\"#f1c40f\",\"menu_mode\":\"inline\",\"custom_css\":\"\"}');

-- --------------------------------------------------------

--
-- Table structure for table `utopigs_seo_data`
--

CREATE TABLE `utopigs_seo_data` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(191) NOT NULL,
  `reference` varchar(191) NOT NULL,
  `title` varchar(191) NOT NULL,
  `description` text DEFAULT NULL,
  `keywords` varchar(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `utopigs_seo_data`
--

INSERT INTO `utopigs_seo_data` (`id`, `type`, `reference`, `title`, `description`, `keywords`, `created_at`, `updated_at`) VALUES
(1, 'cms-page', 'trang-chu', 'Khoa Răng Hàm Mặt', 'Khoa Răng Hàm Mặt', '', '2022-12-08 09:16:53', '2022-12-08 09:16:53');

-- --------------------------------------------------------

--
-- Table structure for table `utopigs_seo_sitemaps`
--

CREATE TABLE `utopigs_seo_sitemaps` (
  `id` int(10) UNSIGNED NOT NULL,
  `theme` varchar(191) DEFAULT NULL,
  `data` mediumtext DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `utopigs_seo_sitemaps`
--

INSERT INTO `utopigs_seo_sitemaps` (`id`, `theme`, `data`, `created_at`, `updated_at`) VALUES
(1, 'custom', '[]', '2022-12-07 07:03:01', '2022-12-07 07:03:01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alipo_about_posts`
--
ALTER TABLE `alipo_about_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_announce_posts`
--
ALTER TABLE `alipo_announce_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_blog_categories`
--
ALTER TABLE `alipo_blog_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_blog_category_posts`
--
ALTER TABLE `alipo_blog_category_posts`
  ADD PRIMARY KEY (`post_id`,`category_id`);

--
-- Indexes for table `alipo_blog_posts`
--
ALTER TABLE `alipo_blog_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_cms_aboutpages`
--
ALTER TABLE `alipo_cms_aboutpages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_cms_contactpages`
--
ALTER TABLE `alipo_cms_contactpages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_cms_contact_infos`
--
ALTER TABLE `alipo_cms_contact_infos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_cms_faqpages`
--
ALTER TABLE `alipo_cms_faqpages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_cms_generals`
--
ALTER TABLE `alipo_cms_generals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_cms_homepages`
--
ALTER TABLE `alipo_cms_homepages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_cms_htqts`
--
ALTER TABLE `alipo_cms_htqts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_cms_nckhs`
--
ALTER TABLE `alipo_cms_nckhs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_cms_newspages`
--
ALTER TABLE `alipo_cms_newspages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_cms_servicepages`
--
ALTER TABLE `alipo_cms_servicepages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_cms_students`
--
ALTER TABLE `alipo_cms_students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_cms_teampages`
--
ALTER TABLE `alipo_cms_teampages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_subject_posts`
--
ALTER TABLE `alipo_subject_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alipo_training_posts`
--
ALTER TABLE `alipo_training_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `backend_access_log`
--
ALTER TABLE `backend_access_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `backend_users`
--
ALTER TABLE `backend_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login_unique` (`login`),
  ADD UNIQUE KEY `email_unique` (`email`),
  ADD KEY `act_code_index` (`activation_code`),
  ADD KEY `reset_code_index` (`reset_password_code`),
  ADD KEY `admin_role_index` (`role_id`);

--
-- Indexes for table `backend_users_groups`
--
ALTER TABLE `backend_users_groups`
  ADD PRIMARY KEY (`user_id`,`user_group_id`);

--
-- Indexes for table `backend_user_groups`
--
ALTER TABLE `backend_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_unique` (`name`),
  ADD KEY `code_index` (`code`);

--
-- Indexes for table `backend_user_preferences`
--
ALTER TABLE `backend_user_preferences`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_item_index` (`user_id`,`namespace`,`group`,`item`);

--
-- Indexes for table `backend_user_roles`
--
ALTER TABLE `backend_user_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `role_unique` (`name`),
  ADD KEY `role_code_index` (`code`);

--
-- Indexes for table `backend_user_throttle`
--
ALTER TABLE `backend_user_throttle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `backend_user_throttle_user_id_index` (`user_id`),
  ADD KEY `backend_user_throttle_ip_address_index` (`ip_address`);

--
-- Indexes for table `benfreke_menumanager_menus`
--
ALTER TABLE `benfreke_menumanager_menus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `benfreke_menumanager_menus_parent_id_index` (`parent_id`);

--
-- Indexes for table `cache`
--
ALTER TABLE `cache`
  ADD UNIQUE KEY `cache_key_unique` (`key`);

--
-- Indexes for table `cms_theme_data`
--
ALTER TABLE `cms_theme_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cms_theme_data_theme_index` (`theme`);

--
-- Indexes for table `cms_theme_logs`
--
ALTER TABLE `cms_theme_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cms_theme_logs_type_index` (`type`),
  ADD KEY `cms_theme_logs_theme_index` (`theme`),
  ADD KEY `cms_theme_logs_user_id_index` (`user_id`);

--
-- Indexes for table `cms_theme_templates`
--
ALTER TABLE `cms_theme_templates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cms_theme_templates_source_index` (`source`),
  ADD KEY `cms_theme_templates_path_index` (`path`);

--
-- Indexes for table `deferred_bindings`
--
ALTER TABLE `deferred_bindings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `deferred_bindings_master_type_index` (`master_type`),
  ADD KEY `deferred_bindings_master_field_index` (`master_field`),
  ADD KEY `deferred_bindings_slave_type_index` (`slave_type`),
  ADD KEY `deferred_bindings_slave_id_index` (`slave_id`),
  ADD KEY `deferred_bindings_session_key_index` (`session_key`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_reserved_at_index` (`queue`,`reserved_at`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rainlab_translate_attributes`
--
ALTER TABLE `rainlab_translate_attributes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rainlab_translate_attributes_locale_index` (`locale`),
  ADD KEY `rainlab_translate_attributes_model_id_index` (`model_id`),
  ADD KEY `rainlab_translate_attributes_model_type_index` (`model_type`);

--
-- Indexes for table `rainlab_translate_indexes`
--
ALTER TABLE `rainlab_translate_indexes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rainlab_translate_indexes_locale_index` (`locale`),
  ADD KEY `rainlab_translate_indexes_model_id_index` (`model_id`),
  ADD KEY `rainlab_translate_indexes_model_type_index` (`model_type`),
  ADD KEY `rainlab_translate_indexes_item_index` (`item`);

--
-- Indexes for table `rainlab_translate_locales`
--
ALTER TABLE `rainlab_translate_locales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rainlab_translate_locales_code_index` (`code`),
  ADD KEY `rainlab_translate_locales_name_index` (`name`);

--
-- Indexes for table `rainlab_translate_messages`
--
ALTER TABLE `rainlab_translate_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rainlab_translate_messages_code_index` (`code`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `system_event_logs`
--
ALTER TABLE `system_event_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_event_logs_level_index` (`level`);

--
-- Indexes for table `system_files`
--
ALTER TABLE `system_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_files_field_index` (`field`),
  ADD KEY `system_files_attachment_id_index` (`attachment_id`),
  ADD KEY `system_files_attachment_type_index` (`attachment_type`);

--
-- Indexes for table `system_mail_layouts`
--
ALTER TABLE `system_mail_layouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_mail_partials`
--
ALTER TABLE `system_mail_partials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_mail_templates`
--
ALTER TABLE `system_mail_templates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_mail_templates_layout_id_index` (`layout_id`);

--
-- Indexes for table `system_parameters`
--
ALTER TABLE `system_parameters`
  ADD PRIMARY KEY (`id`),
  ADD KEY `item_index` (`namespace`,`group`,`item`);

--
-- Indexes for table `system_plugin_history`
--
ALTER TABLE `system_plugin_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_plugin_history_code_index` (`code`),
  ADD KEY `system_plugin_history_type_index` (`type`);

--
-- Indexes for table `system_plugin_versions`
--
ALTER TABLE `system_plugin_versions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_plugin_versions_code_index` (`code`);

--
-- Indexes for table `system_request_logs`
--
ALTER TABLE `system_request_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_revisions`
--
ALTER TABLE `system_revisions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_revisions_revisionable_id_revisionable_type_index` (`revisionable_id`,`revisionable_type`),
  ADD KEY `system_revisions_user_id_index` (`user_id`),
  ADD KEY `system_revisions_field_index` (`field`);

--
-- Indexes for table `system_settings`
--
ALTER TABLE `system_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_settings_item_index` (`item`);

--
-- Indexes for table `utopigs_seo_data`
--
ALTER TABLE `utopigs_seo_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `utopigs_seo_sitemaps`
--
ALTER TABLE `utopigs_seo_sitemaps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `utopigs_seo_sitemaps_theme_index` (`theme`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alipo_about_posts`
--
ALTER TABLE `alipo_about_posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `alipo_announce_posts`
--
ALTER TABLE `alipo_announce_posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `alipo_blog_categories`
--
ALTER TABLE `alipo_blog_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `alipo_blog_posts`
--
ALTER TABLE `alipo_blog_posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `alipo_cms_aboutpages`
--
ALTER TABLE `alipo_cms_aboutpages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `alipo_cms_contactpages`
--
ALTER TABLE `alipo_cms_contactpages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `alipo_cms_contact_infos`
--
ALTER TABLE `alipo_cms_contact_infos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `alipo_cms_faqpages`
--
ALTER TABLE `alipo_cms_faqpages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `alipo_cms_generals`
--
ALTER TABLE `alipo_cms_generals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `alipo_cms_homepages`
--
ALTER TABLE `alipo_cms_homepages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `alipo_cms_htqts`
--
ALTER TABLE `alipo_cms_htqts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `alipo_cms_nckhs`
--
ALTER TABLE `alipo_cms_nckhs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `alipo_cms_newspages`
--
ALTER TABLE `alipo_cms_newspages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `alipo_cms_servicepages`
--
ALTER TABLE `alipo_cms_servicepages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `alipo_cms_students`
--
ALTER TABLE `alipo_cms_students`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `alipo_cms_teampages`
--
ALTER TABLE `alipo_cms_teampages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `alipo_subject_posts`
--
ALTER TABLE `alipo_subject_posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `alipo_training_posts`
--
ALTER TABLE `alipo_training_posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `backend_access_log`
--
ALTER TABLE `backend_access_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `backend_users`
--
ALTER TABLE `backend_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `backend_user_groups`
--
ALTER TABLE `backend_user_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `backend_user_preferences`
--
ALTER TABLE `backend_user_preferences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `backend_user_roles`
--
ALTER TABLE `backend_user_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `backend_user_throttle`
--
ALTER TABLE `backend_user_throttle`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `benfreke_menumanager_menus`
--
ALTER TABLE `benfreke_menumanager_menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `cms_theme_data`
--
ALTER TABLE `cms_theme_data`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_theme_logs`
--
ALTER TABLE `cms_theme_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_theme_templates`
--
ALTER TABLE `cms_theme_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `deferred_bindings`
--
ALTER TABLE `deferred_bindings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `rainlab_translate_attributes`
--
ALTER TABLE `rainlab_translate_attributes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `rainlab_translate_indexes`
--
ALTER TABLE `rainlab_translate_indexes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rainlab_translate_locales`
--
ALTER TABLE `rainlab_translate_locales`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `rainlab_translate_messages`
--
ALTER TABLE `rainlab_translate_messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `system_event_logs`
--
ALTER TABLE `system_event_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `system_files`
--
ALTER TABLE `system_files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `system_mail_layouts`
--
ALTER TABLE `system_mail_layouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `system_mail_partials`
--
ALTER TABLE `system_mail_partials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_mail_templates`
--
ALTER TABLE `system_mail_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_parameters`
--
ALTER TABLE `system_parameters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `system_plugin_history`
--
ALTER TABLE `system_plugin_history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=199;

--
-- AUTO_INCREMENT for table `system_plugin_versions`
--
ALTER TABLE `system_plugin_versions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `system_request_logs`
--
ALTER TABLE `system_request_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_revisions`
--
ALTER TABLE `system_revisions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_settings`
--
ALTER TABLE `system_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `utopigs_seo_data`
--
ALTER TABLE `utopigs_seo_data`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `utopigs_seo_sitemaps`
--
ALTER TABLE `utopigs_seo_sitemaps`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
