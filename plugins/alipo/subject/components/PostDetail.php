<?php namespace Alipo\Subject\Components;

use Cms\Classes\ComponentBase;
use Alipo\Subject\Models\Post;

class PostDetail extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'PostDetail Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
    public function onRun() {
        parent::onRun(); // TODO: Change the autogenerated stub
        $this->page['subjectDetail'] = $this->loadPost();        
      

    }


    protected function loadPost()
    {
        $slug = $this->param('slug');
        $post = new Post;

        $post = $post->where('slug', $slug);

        $post = $post->first();

        return $post;
    }
}
