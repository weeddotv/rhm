<?php namespace Alipo\Subject;

use Backend;
use BackendMenu;
use System\Classes\PluginBase;

/**
 * Subject Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Subject',
            'description' => 'No description provided yet...',
            'author'      => 'Alipo',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        BackendMenu::registerContextSidenavPartial('Alipo.Subject', 'subject', 'plugins/alipo/subject/partials/sidebar');

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {

        return [
            'Alipo\Subject\Components\PostCp' => 'SubjectComponent',
            'Alipo\Subject\Components\PostDetail' => 'SubjectDetailComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'alipo.subject.some_permission' => [
                'tab' => 'Subject',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        // return []; // Remove this line to activate

        return [
            'subject' => [
                'label'       => 'Bộ môn',
                'url'         => Backend::url('alipo/subject/post'),
                'icon'        => 'icon-leaf',
                'permissions' => ['alipo.subject.*'],
                'order'       => 500,
                'sideMenu' => [
                    'post' => [
                        'label'       => 'Post',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/subject/post/'),
                        'permissions' => ['alipo.subject.post'],
                        'group'       => 'Post'
                    ],
                ]
            ],
        ];
    }
}
