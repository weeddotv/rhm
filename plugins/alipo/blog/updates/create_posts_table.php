<?php namespace Alipo\Blog\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePostsTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('alipo_blog_posts')){ 
            Schema::create('alipo_blog_posts', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('title');
                $table->text('slug');
                $table->text('short_des');
                $table->text('des');
                $table->timestamps();
            });
        }

    }

    public function down()
    {
        Schema::dropIfExists('alipo_blog_posts');
    }
}
