<?php namespace Alipo\NCKH;

use Backend;
use BackendMenu;
use System\Classes\PluginBase;

/**
 * NCKH Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'NCKH',
            'description' => 'No description provided yet...',
            'author'      => 'Alipo',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        BackendMenu::registerContextSidenavPartial('Alipo.NCKH', 'nckh', 'plugins/alipo/nckh/partials/sidebar');

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {

        return [
            'Alipo\NCKH\Components\Post' => 'NCKHComponent',
            'Alipo\NCKH\Components\PostDetail' => 'NCKHDetailComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'alipo.nckh.some_permission' => [
                'tab' => 'NCKH',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {

        return [
            'nckh' => [
                'label'       => 'NCKH',
                'url'         => Backend::url('alipo/nckh/post'),
                'icon'        => 'icon-leaf',
                'permissions' => ['alipo.nckh.*'],
                'order'       => 500,
                'sideMenu' => [
                    'post' => [
                        'label'       => 'Post',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/nckh/post/'),
                        'permissions' => ['alipo.nckh.post'],
                        'group'       => 'Post'
                    ],
                    'category' => [
                        'label'       => 'Category',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/nckh/category/'),
                        'permissions' => ['alipo.nckh.cateogry'],
                        'group'       => 'Post'
                    ],
                ]
            ],
        ];
    }
}
