<?php namespace Alipo\SVHV;

use Backend;
use BackendMenu;
use System\Classes\PluginBase;

/**
 * SVHV Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'SVHV',
            'description' => 'No description provided yet...',
            'author'      => 'Alipo',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        BackendMenu::registerContextSidenavPartial('Alipo.SVHV', 'svhv', 'plugins/alipo/svhv/partials/sidebar');

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {

        return [
            'Alipo\SVHV\Components\Post' => 'SVHVComponent',
            'Alipo\SVHV\Components\PostDetail' => 'SVHVDetailComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'alipo.svhv.some_permission' => [
                'tab' => 'SVHV',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {

        return [
            'svhv' => [
                'label'       => 'SVHV',
                'url'         => Backend::url('alipo/svhv/post'),
                'icon'        => 'icon-leaf',
                'permissions' => ['alipo.svhv.*'],
                'order'       => 500,
                'sideMenu' => [
                    'post' => [
                        'label'       => 'Post',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/svhv/post/'),
                        'permissions' => ['alipo.svhv.post'],
                        'group'       => 'Post'
                    ],
                    'category' => [
                        'label'       => 'Category',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/svhv/category/'),
                        'permissions' => ['alipo.svhv.cateogry'],
                        'group'       => 'Post'
                    ],
                ]
            ],
        ];
    }
}
