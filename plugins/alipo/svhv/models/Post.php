<?php namespace Alipo\SVHV\Models;

use Model;

/**
 * Post Model
 */
class Post extends Model
{
    use \October\Rain\Database\Traits\Validation;
    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];
    public $translatable = [
        'title',
        'short_des',
        'des',
    ];
    /**
     * @var string The database table used by the model.
     */
    public $table = 'alipo_svhv_posts';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Validation rules for attributes
     */
    public $rules = [
        'title'    => 'required',
        'short_des' => 'required',
        'des' => 'required',
        'thumb' => 'required',
        'slug' => 'required',
    ];


    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array Attributes to be cast to JSON
     */
    protected $jsonable = [];

    /**
     * @var array Attributes to be appended to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [
        'categories' => [
            'Alipo\SVHV\Models\Category',
            'table'    => 'alipo_svhv_category_posts',
            'key'      => 'post_id',
        ]
    ];   
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = ['thumb' =>  'System\Models\File'];
    public $attachMany = [];
}
