<?php namespace Alipo\SVHV\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateCategoriesTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('alipo_svhv_categories')){ 
            Schema::create('alipo_svhv_categories', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('name');
                $table->text('slug');
                $table->timestamps();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('alipo_svhv_categories');
    }
}
