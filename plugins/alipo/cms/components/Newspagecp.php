<?php namespace Alipo\Cms\Components;

use Cms\Classes\ComponentBase;
use Alipo\Cms\Models\Newspage;

class Newspagecp extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Newspagecp Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
    public function onRun() {
        parent::onRun(); // TODO: Change the autogenerated stub
        $this->page['newspage'] = Newspage::first();
    }
}
