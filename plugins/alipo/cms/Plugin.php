<?php namespace Alipo\Cms;

use Backend;
use BackendMenu;
use System\Classes\PluginBase;

/**
 * Cms Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Cms',
            'description' => 'No description provided yet...',
            'author'      => 'Alipo',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        BackendMenu::registerContextSidenavPartial('Alipo.Cms', 'cms', 'plugins/alipo/cms/partials/sidebar');

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        // return []; // Remove this line to activate

        return [
            'Alipo\Cms\Components\Aboutpagecp' => 'AboutpageComponent',
            'Alipo\Cms\Components\Contactpagecp' => 'ContactpageComponent',
            'Alipo\Cms\Components\Homepagecp' => 'HomepageComponent',
            'Alipo\Cms\Components\NckhCp' => 'NckhComponent',
            'Alipo\Cms\Components\HtqtCp' => 'HtqtComponent',
            'Alipo\Cms\Components\Student' => 'StudentComponent',

        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'alipo.cms.some_permission' => [
                'tab' => 'Cms',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        // return []; // Remove this line to activate

        return [
            'cms' => [
                'label'       => 'Trang',
                'url'         => Backend::url('alipo/cms/general/update/1'),
                'icon'        => 'icon-leaf',
                'permissions' => ['alipo.cms.*'],
                'order'       => 500,
                'sideMenu' => [
                    'general' => [
                        'label'       => 'Thông tin chung',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/cms/general/update/1'),
                        'permissions' => ['alipo.cms.general'],
                        'group'       => 'General'
                    ],
                    'contactinfo' => [
                        'label'       => 'Khách hàng liên hệ',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/cms/contactinfo'),
                        'permissions' => ['alipo.cms.*'],
                        'group'       => 'General'
                    ],
                    'homepage' => [
                        'label'       => 'Trang chủ',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/cms/homepage/update/1'),
                        'permissions' => ['alipo.cms.homepage'],
                        'group'       => 'Pages'
                    ],
                    'aboutpage' => [
                        'label'       => 'Giới thiệu',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/cms/aboutpage/update/1'),
                        'permissions' => ['alipo.cms.aboutpage'],
                        'group'       => 'Pages'
                    ],                    

                    'nckh' => [
                        'label'       => 'NCKH',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/cms/nckh/update/1'),
                        'permissions' => ['alipo.cms.nckh'],
                        'group'       => 'Pages'
                    ],  
                    'htqt' => [
                        'label'       => 'HTQT',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/cms/htqt/update/1'),
                        'permissions' => ['alipo.cms.htqt'],
                        'group'       => 'Pages'
                    ],  
                    'student' => [
                        'label'       => 'HS-SV',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/cms/student/update/1'),
                        'permissions' => ['alipo.cms.student'],
                        'group'       => 'Pages'
                    ],  
                ]
            ],
        ];
    }
}
