<?php namespace Alipo\Cms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateContactInfosTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('alipo_cms_contact_infos')){ 
            Schema::create('alipo_cms_contact_infos', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('name');
                $table->text('email');
                $table->text('subject');
                $table->text('phone');
                $table->text('message');
                $table->timestamps();
            });
        }

    }

    public function down()
    {
        Schema::dropIfExists('alipo_cms_contact_infos');
    }
}
