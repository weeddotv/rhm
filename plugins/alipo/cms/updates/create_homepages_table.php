<?php namespace Alipo\Cms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateHomepagesTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('alipo_cms_homepages')){ 
            Schema::create('alipo_cms_homepages', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('slider');
                $table->text('contact_title');
                $table->text('contact_des');
                $table->text('intro_title');
                $table->text('intro_des');
                $table->text('student_info');
                $table->timestamps();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('alipo_cms_homepages');
    }
}
