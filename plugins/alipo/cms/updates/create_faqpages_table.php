<?php namespace Alipo\Cms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateFaqpagesTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('alipo_cms_faqpages')){ 
            Schema::create('alipo_cms_faqpages', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('title');
                $table->text('intro_title');
                $table->text('instro_des');
                $table->text('quote');
                $table->timestamps();
            });
        }

    }

    public function down()
    {
        Schema::dropIfExists('alipo_cms_faqpages');
    }
}
