<?php namespace Alipo\Cms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateServicepagesTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('alipo_cms_servicepages')){ 
            Schema::create('alipo_cms_servicepages', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('title');
                $table->text('intro_des');
                $table->text('service_title');
                $table->text('service_des');
                $table->text('stats');
                $table->timestamps();
            });
        }

    }

    public function down()
    {
        Schema::dropIfExists('alipo_cms_servicepages');
    }
}
