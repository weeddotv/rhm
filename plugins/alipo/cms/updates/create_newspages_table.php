<?php namespace Alipo\Cms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateNewspagesTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('alipo_cms_newspages')){ 
            Schema::create('alipo_cms_newspages', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('title');
                $table->timestamps();
            });
        }

    }

    public function down()
    {
        Schema::dropIfExists('alipo_cms_newspages');
    }
}
