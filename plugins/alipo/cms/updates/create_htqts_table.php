<?php namespace Alipo\Cms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateHtqtsTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('alipo_cms_htqts')){ 
            Schema::create('alipo_cms_htqts', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('description');
                $table->timestamps();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('alipo_cms_htqts');
    }
}
