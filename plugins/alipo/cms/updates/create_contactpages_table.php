<?php namespace Alipo\Cms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateContactpagesTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('alipo_cms_contactpages')){ 
            Schema::create('alipo_cms_contactpages', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('title');
                $table->timestamps();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('alipo_cms_contactpages');
    }
}
