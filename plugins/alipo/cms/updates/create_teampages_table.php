<?php namespace Alipo\Cms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateTeampagesTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('alipo_cms_teampages')){ 
            Schema::create('alipo_cms_teampages', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('title');
                $table->text('team_title');
                $table->text('team_subtitle');
                $table->text('team_des');
                $table->timestamps();
            });
        }

    }

    public function down()
    {
        Schema::dropIfExists('alipo_cms_teampages');
    }
}
