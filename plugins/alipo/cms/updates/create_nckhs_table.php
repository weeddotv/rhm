<?php namespace Alipo\Cms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateNckhsTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('alipo_cms_nckhs')){ 
            Schema::create('alipo_cms_nckhs', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('description');
                $table->timestamps();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('alipo_cms_nckhs');
    }
}
