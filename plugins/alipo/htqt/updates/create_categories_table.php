<?php namespace Alipo\HTQT\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateCategoriesTable extends Migration
{
    public function up()
    {

        if(!Schema::hasTable('alipo_htqt_categories')){ 
            Schema::create('alipo_htqt_categories', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('name');
                $table->text('slug');
                $table->timestamps();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('alipo_htqt_categories');
    }
}
