<?php namespace Alipo\HTQT;

use Backend;
use BackendMenu;
use System\Classes\PluginBase;

/**
 * HTQT Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'HTQT',
            'description' => 'No description provided yet...',
            'author'      => 'Alipo',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        BackendMenu::registerContextSidenavPartial('Alipo.HTQT', 'htqt', 'plugins/alipo/htqt/partials/sidebar');

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {

        return [
            'Alipo\HTQT\Components\Post' => 'HTQTComponent',
            'Alipo\HTQT\Components\PostDetail' => 'HTQTDetailComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'alipo.htqt.some_permission' => [
                'tab' => 'HTQT',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {

        return [
            'htqt' => [
                'label'       => 'HTQT',
                'url'         => Backend::url('alipo/htqt/post'),
                'icon'        => 'icon-leaf',
                'permissions' => ['alipo.htqt.*'],
                'order'       => 500,
                'sideMenu' => [
                    'post' => [
                        'label'       => 'Post',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/htqt/post/'),
                        'permissions' => ['alipo.htqt.post'],
                        'group'       => 'Post'
                    ],
                    'category' => [
                        'label'       => 'Category',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/htqt/category/'),
                        'permissions' => ['alipo.htqt.cateogry'],
                        'group'       => 'Post'
                    ],
                ]
            ],
        ];
    }
}
