<?php namespace Alipo\Announce;

use System\Classes\PluginBase;
use Backend;
use BackendMenu;

/**
 * Announce Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Announce',
            'description' => 'No description provided yet...',
            'author'      => 'Alipo',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        BackendMenu::registerContextSidenavPartial('Alipo.Announce', 'announce', 'plugins/alipo/announce/partials/sidebar');

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        // return []; // Remove this line to activate

        return [
            'Alipo\Announce\Components\PostCp' => 'AnnounceComponent',
            'Alipo\Announce\Components\PostDetailCp' => 'AnnounceDetailComponent',

        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'alipo.announce.some_permission' => [
                'tab' => 'Announce',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        // return []; // Remove this line to activate

        return [
            'announce' => [
                'label'       => 'Thông báo',
                'url'         => Backend::url('alipo/announce/post'),
                'icon'        => 'icon-leaf',
                'permissions' => ['alipo.announce.*'],
                'order'       => 500,
                'sideMenu' => [
                    'post' => [
                        'label'       => 'Post',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/announce/post/'),
                        'permissions' => ['alipo.announce.post'],
                        'group'       => 'Post' 
                    ],
                    'category' => [
                        'label'       => 'Category',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/announce/category/'),
                        'permissions' => ['alipo.announce.cateogry'],
                        'group'       => 'Post'
                    ],
                ]
            ],
        ];
    }
}
