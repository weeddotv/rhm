<?php namespace Alipo\Training;

use Backend;
use BackendMenu;
use System\Classes\PluginBase;

/**
 * Training Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Training',
            'description' => 'No description provided yet...',
            'author'      => 'Alipo',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        BackendMenu::registerContextSidenavPartial('Alipo.Training', 'training', 'plugins/alipo/training/partials/sidebar');

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {

        return [
            'Alipo\Training\Components\PostCp' => 'TrainingComponent',
            'Alipo\Training\Components\PostDetail' => 'TrainingDetailComponent',

        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'alipo.training.some_permission' => [
                'tab' => 'Training',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {

        return [
            'training' => [
                'label'       => 'Đào Tạo',
                'url'         => Backend::url('alipo/training/post'),
                'icon'        => 'icon-leaf',
                'permissions' => ['alipo.training.*'],
                'order'       => 500,
                'sideMenu' => [
                    'post' => [
                        'label'       => 'Post',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/training/post/'),
                        'permissions' => ['alipo.training.post'],
                        'group'       => 'Post'
                    ],

                ]
            ],
        ];
    }
}
