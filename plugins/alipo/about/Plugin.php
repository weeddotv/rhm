<?php namespace Alipo\About;

use Backend;
use BackendMenu;
use System\Classes\PluginBase;

/**
 * About Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'About',
            'description' => 'No description provided yet...',
            'author'      => 'Alipo',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        BackendMenu::registerContextSidenavPartial('Alipo.About', 'about', 'plugins/alipo/about/partials/sidebar');
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {

        return [
            'Alipo\About\Components\PostCp' => 'AboutComponent',
            'Alipo\About\Components\PostDetailCp' => 'AboutDetailComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'alipo.about.some_permission' => [
                'tab' => 'About',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {

        return [
            'about' => [
                'label'       => 'Giới thiệu',
                'url'         => Backend::url('alipo/about/post'),
                'icon'        => 'icon-leaf',
                'permissions' => ['alipo.about.*'],
                'order'       => 500,
                'sideMenu' => [
                    'post' => [
                        'label'       => 'Post',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/about/post/'),
                        'permissions' => ['alipo.about.post'],
                        'group'       => 'Post'
                    ],

                ]
            ],
        ];
    }
}
