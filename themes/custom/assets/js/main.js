$(document).ready(function () {
    $(".js-quote button").on("click", function () {
        $(this).parent().addClass("isActive");
        $(this).parent().siblings().removeClass("isActive");
    });
    $(".hamburger").on("click", function () {
        if ($("body").hasClass("open-nav")) {
            $("body").removeClass("open-nav");
        } else {
            $("body").addClass("open-nav");
        }
    });

    $(".hero-slider-one").slick({
        dots: false,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: "linear",
        prevArrow: false,
        nextArrow: false,
        autoplay: true,
        autoplaySpeed: 6000,
    });
});
$(document).mouseup(function (e) {
    var container = $(".dr-nav-pills");

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0) {
        $("body").removeClass("open-nav");
    }
});
